// Importing AmCharts
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import * as am4maps from "@amcharts/amcharts4/maps";

// Importing themes
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import am4themes_dataviz from "@amcharts/amcharts4/themes/dataviz";
import am4themes_material from "@amcharts/amcharts4/themes/material";

// Importing geodata (map data)
import am4geodata_worldLow from "@amcharts/amcharts4-geodata/worldHigh";

var $ = jQuery;
// Data for general and map use
var originCountries = require('./json_data/origincountries.json');
var destinationCountries = require('./json_data/destinationcountries.json');
var json = require('./json_data/data.json');


// Themes begin
am4core.useTheme(am4themes_animated);
am4core.useTheme(am4themes_dataviz);
am4core.options.autoSetClassName = true;
am4core.useTheme(am4themes_material);
// Themes end

//Country default
var country = "";

if (detectmob()){
    window.mySwipe = new Swipe(document.getElementById('sliderD'), {
        startSlide: 0,
        speed: 400,
        auto: 0,
        draggable: false,
        continuous: false,
        disableScroll: false,
        stopPropagation: true,
        callback: function(index, elem, dir) {
        },
        transitionEnd: function(index, elem) {

        }
    });
    window.mySwipe.disable();
    $("#sliderD").css('visibility', 'visible');
    $("#sliderO").css('visibility', 'visible');
}else {
    $("#sliderD").css('visibility', 'visible');
    $("#sliderO").css('visibility', 'visible');
}

//State default
var state = "destination";
var bugLibrary = true;
var coords_sing = [];
var countryB = "";
var countryName;
var onCountry = false;
var left;
var top;
var left_info;
var top_info;
var selectedCountry = '';
var singapore = getOriginCountry("SG");
var runUpdate = true;
var savedCode = '';
let gradientOrigin = new am4core.LinearGradient();
gradientOrigin.addColor(am4core.color("#2096F3"));
gradientOrigin.addColor(am4core.color("#27B870"));

let gradientOriginR = new am4core.LinearGradient();
gradientOriginR.addColor(am4core.color("#27B870"));
gradientOriginR.addColor(am4core.color("#2096F3"));

let gradientDestination = new am4core.LinearGradient();
gradientDestination.addColor(am4core.color("#4EC287"));
gradientDestination.addColor(am4core.color("#52C6D8"));

for (var data2 in json) {
    countryName = json[5].country;
}

if(state == 'destination'){
    document.body.style.backgroundColor = "#FFF";
}else{
    document.body.style.backgroundColor = "#363a45";
}

(function($){
    $('document').ready(function(){
        // Origin Content
        function contentOrigin(state, iso){
            for( var data in json) {
                if(json[data].iso === iso){
                    //clean slides
                    $(".carousel-indicators").html("");
                    $(".carousel-inner").html("");

                    //GRAB TRAVELLER
                    //add slides
                    if (json[data].origin.pax.length === 0){
                        $("#GTA .box-content").html("<div class=\"details\">"+
                            "<div class=\"percentage\">"+
                            "<div class=\"number\">"+ json[data].origin.residents +"</div>"+
                            "<div class=\"signal\">%</div>"+
                            "</div>"+
                            "<div class= \"text\">of all overseas bookings were by " +
                            ((json[data].country === "Philippines") ? 'the ' : '') +
                            json[data].country + " Grab users" +
                            "</div>"
                        );

                        $("#modalA1 .box-content").html("<div class=\"details\">"+
                            "<div class=\"percentage\">"+
                            "<div class=\"number\">"+ json[data].origin.residents +"</div>"+
                            "<div class=\"signal\">%</div>"+
                            "</div>"+
                            "<div class= \"text\">of all overseas bookings were by  "+ json[data].country + " Grab users" + "</div>"
                        );
                    }

                    for( var k = 0; k < json[data].origin.pax.length; k++){
                        //add indicators
                        if(json[data].origin.pax.length > 1){
                            $("#myCarouselA1 .carousel-indicators").append("<li data-target=\"#myCarouselA1\" data-slide-to=\"" + k + "\"></li>");
                            $("#myCarouselMA1 .carousel-indicators").append("<li data-target=\"#myCarouselMA1\" data-slide-to=\"" + k + "\"></li>");
                        }

                        $("#GTA .box-content").html("<div class=\"details\">"+
                            "<div class=\"percentage\">"+
                            "<div class=\"number\">"+ json[data].origin.residents +"</div>"+
                            "<div class=\"signal\">%</div>"+
                            "</div>"+
                            "<div class= \"text\">of all overseas bookings were by " +
                            ((json[data].country === "Philippines") ? 'the ' : '') +
                            json[data].country + " Grab users" +
                            "</div>"
                        );

                        $("#modalA1 .box-content").html("<div class=\"details\">"+
                            "<div class=\"percentage\">"+
                            "<div class=\"number\">"+ json[data].origin.residents +"</div>"+
                            "<div class=\"signal\">%</div>"+
                            "</div>"+
                            "<div class= \"text\">of all overseas bookings were by " +
                            ((json[data].country === "Philippines") ? 'the ' : '') +
                            json[data].country + " Grab users" +
                            "</div>"
                        );

                        //add content slides
                        $("#myCarouselA1 .carousel-inner").append(
                            "<div class=\"carousel-item item\">" + pax() + "</div>"
                        );
                        $("#myCarouselMA1 .carousel-inner").append(
                            "<div class=\"carousel-item item\">" + pax() + "</div>"
                        );

                        //add state active the first item
                        if(k == 0){
                            $("#myCarouselA1 .carousel-indicators li").addClass("active");
                            $("#myCarouselA1 .carousel-inner .item").addClass("active");
                            $("#myCarouselMA1 .carousel-indicators li").addClass("active");
                            $("#myCarouselMA1 .carousel-inner .item").addClass("active");
                        }
                        // add paxs
                        function pax(){
                            for(var g = 0 ; g < json[data].origin.pax.length; g++){
                                if(g == k){
                                    return "<div class=\"quote\">"+json[data].origin.pax[g].text +"</div><div class=\"author\">"+ json[data].origin.pax[g].author + "</div>";
                                }
                            }
                        }
                    }

                    // GRAB OUTBOUND
                    $("#GTA2 .box-content").html(

                        "<div class=\"text\">"+ json[data].origin.statistics +"</div><br/>"+

                        "<div class=\"text\">"+ json[data].origin.spending +"</div>"
                    );
                    $("#modalA2 .box-content").html(

                        "<div class=\"text\">"+ json[data].origin.statistics +"</div><br/>"+

                        "<div class=\"text\">"+ json[data].origin.spending +"</div>"
                    );


                    // STORY HIGHLIGHTS
                    // Add Videos Slides
                    for( var k = 0; k < json[data].videos.length; k++){

                        // Add indicators
                        if(json[data].videos.length > 0){
                            $("#myCarouselA2 .carousel-indicators").append("<li data-target=\"#myCarouselA2\" data-slide-to=\"" + k + "\"></li>");
                            $("#myCarouselMA2 .carousel-indicators").append("<li data-target=\"#myCarouselMA2\" data-slide-to=\"" + k + "\"></li>");
                        }

                        // Add content slides
                        $("#myCarouselA2 .carousel-inner").append(
                            "<div class=\"carousel-item item \">"+videos()+"</div>"
                        );
                        $("#myCarouselMA2 .carousel-inner").append(
                            "<div class=\"carousel-item item \">"+videos()+"</div>"
                        );

                        // Add state active the first item
                        if(k === 0){
                            $("#myCarouselA2 .carousel-indicators li").addClass("active");
                            $("#myCarouselA2 .carousel-inner .item").addClass("active");
                            $("#myCarouselMA2 .carousel-indicators li").addClass("active");
                            $("#myCarouselMA2 .carousel-inner .item").addClass("active");
                        }

                        // Add videos
                        function videos(){
                            for(var g = 0 ; g < json[data].videos.length; g++){
                                if(g == k){
                                    return "<iframe class=\"youtube\" width=\"328\" height=\"163\" src=\"" + json[data].videos[g].link + "\" frameborder=\"0\" allowfullscreen></iframe><div class=\"text-caption\">"+ json[data].videos[g].name +"</div>";
                                }
                            }
                        }
                    }
                    // Add Images Slides
                    for( var l = 0; l < json[data].images.length; l++){

                        // Add indicators
                        if(json[data].images.length > 0){
                            $("#myCarouselA2 .carousel-indicators").append("<li data-target=\"#myCarouselA2\" data-slide-to=\"" + (k+l) + "\"></li>");
                            $("#myCarouselMA2 .carousel-indicators").append("<li data-target=\"#myCarouselMA2\" data-slide-to=\"" + (k+l) + "\"></li>");
                        }

                        // Add content slides
                        $("#myCarouselA2 .carousel-inner").append(
                            "<div class=\"carousel-item item \">"+images()+"</div>"
                        );
                        $("#myCarouselMA2 .carousel-inner").append(
                            "<div class=\"carousel-item item \">"+images()+"</div>"
                        );

                        if(json[data].videos.length === 0 && l === 0){
                            $("#myCarouselA2 .carousel-indicators li").addClass("active");
                            $("#myCarouselA2 .carousel-inner .item").addClass("active");
                            $("#myCarouselMA2 .carousel-indicators li").addClass("active");
                            $("#myCarouselMA2 .carousel-inner .item").addClass("active");
                        }

                        // Add images
                        function images(){
                            for(var g = 0 ; g < json[data].images.length; g++){
                                if(g == l){
                                    return "<a href=\"" + json[data].images[g].link  + " \" target=\"_blank\" ><img src=\"" + json[data].images[g].src  + " \" alt=\"\"><\a><div class=\"text-caption\">" + json[data].images[g].text +"</div>";
                                }
                            }
                        }
                    }

                    // GRAB FACTS
                    // Add slides

                    var fact_length = 0;
                    if (json[data].start != ""){
                        fact_length++;
                    }
                    if (json[data].services != ""){
                        fact_length++;
                    }
                    if (json[data].interest != ""){
                        fact_length++;
                    }

                    for (var k = 0; k < fact_length - 1; k++){
                        if (fact_length > 1) {
                            $("#myCarouselA3 .carousel-indicators").append("<li data-target=\"#myCarouselA3\" data-slide-to=\"" + k + "\"></li>");
                            $("#myCarouselMA3 .carousel-indicators").append("<li data-target=\"#myCarouselMA3\" data-slide-to=\"" + k + "\"></li>");
                        }

                        if ( k == 0){
                            $("#myCarouselA3 .carousel-inner").append(
                                "<div class=\"carousel-item item\">" + "<div class='title'>Year started</div><div class='subtitle'>" + json[data].start + "</div><br/><div class='title'>Services available now</div><div class='subtitle'>" + json[data].services + "</div></div>"
                            );
                            $("#myCarouselMA3 .carousel-inner").append(
                                "<div class=\"carousel-item item\">" + "<div class='title'>Year started</div><div class='subtitle'>" + json[data].start + "</div><br/><div class='title'>Services available now</div><div class='subtitle'>" + json[data].services + "</div></div>"
                            );
                        }

                        if (k == 1){
                            if (json[data].interest.link != ""){
                                $("#myCarouselA3 .carousel-inner").append(
                                    "<div class=\"carousel-item item\">" + "<div class='title'>Interesting Factoid</div><div class='subtitle'>" + "<a href=\"" + json[data].interest.link + " \" target=\"_blank\" >" + json[data].interest.text + "</a></div></div>"
                                );
                                $("#myCarouselMA3 .carousel-inner").append(
                                    "<div class=\"carousel-item item\">" + "<div class='title'>Interesting Factoid</div><div class='subtitle'>" + "<a href=\"" + json[data].interest.link + " \" target=\"_blank\" >" + json[data].interest.text + "</a></div></div>"
                                );
                            }else{
                                $("#myCarouselA3 .carousel-inner").append(
                                    "<div class=\"carousel-item item\">" + "<div class='title'>Interesting Factoid</div><div class='subtitle'>" + json[data].interest.text + "</div></div>"
                                );
                                $("#myCarouselMA3 .carousel-inner").append(
                                    "<div class=\"carousel-item item\">" + "<div class='title'>Interesting Factoid</div><div class='subtitle'>" + json[data].interest.text + "</div></div>"
                                );
                            }
                        }

                        if (k == 0){
                            //add state active the first item
                            $("#myCarouselA3 .carousel-indicators li").addClass("active");
                            $("#myCarouselA3 .carousel-inner .item").addClass("active");
                            $("#myCarouselMA3 .carousel-indicators li").addClass("active");
                            $("#myCarouselMA3 .carousel-inner .item").addClass("active");
                        }
                    }
                }
            }
        };

        // Destination Content
        function contentDestination(state, iso){
            for( var data in json) {
                if(json[data].iso === iso){

                    //clean slides
                    $(".carousel-indicators").html("");
                    $(".carousel-inner").html("");

                    //GRAB TRAVELLER
                    //add slides
                    if (json[data].destinations.pax.length === 0){
                        $("#GTB .box-content").html("<div class=\"details\">"+
                            "<div class=\"percentage\">"+
                            "<div class=\"number\">"+ json[data].destinations.residents  +  "</div>"+
                            "<div class=\"signal\">%</div>"+
                            "</div>"+
                            "<div class= \"text\">of all overseas Grab bookings were made in "+
                            ((json[data].country === "Philippines") ? 'the ' : '') +
                            json[data].country +
                            ".</div>"
                        );

                        $("#modalB1 .box-content").html("<div class=\"details\">"+
                            "<div class=\"percentage\">"+
                            "<div class=\"number\">"+ json[data].destinations.residents  +  "</div>"+
                            "<div class=\"signal\">%</div>"+
                            "</div>"+
                            "<div class= \"text\">of all overseas Grab bookings were made in "+ json[data].country + ".</div>"
                        );
                    }

                    for( var k = 0; k < json[data].destinations.pax.length; k++){

                        //add indicators
                        if(json[data].destinations.pax.length > 1){
                            $("#myCarouselB1 .carousel-indicators").append("<li data-target=\"#myCarouselB1\" data-slide-to=\"" + k + "\"></li>");
                            $("#myCarouselMB1 .carousel-indicators").append("<li data-target=\"#myCarouselMB1\" data-slide-to=\"" + k + "\"></li>");
                        }

                        $("#GTB .box-content").html("<div class=\"details\">"+
                            "<div class=\"percentage\">"+
                            "<div class=\"number\">"+ json[data].destinations.residents +"</div>"+
                            "<div class=\"signal\">%</div>"+
                            "</div>"+
                            "<div class= \"text\">of all overseas Grab bookings were made in "+
                            ((json[data].country === "Philippines") ? 'the ' : '') +
                            json[data].country +
                            "</div>"
                        );

                        $("#modalB1 .box-content").html("<div class=\"details\">"+
                            "<div class=\"percentage\">"+
                            "<div class=\"number\">"+ json[data].destinations.residents +"</div>"+
                            "<div class=\"signal\">%</div>"+
                            "</div>"+
                            "<div class= \"text\">of all overseas Grab bookings were made in "+
                            ((json[data].country === "Philippines") ? 'the ' : '') +
                            json[data].country +
                            "</div>"
                        );

                        //add content slides
                        $("#myCarouselB1 .carousel-inner").append(
                            "<div class=\"carousel-item item\">" + pax() + "</div>"
                        );
                        $("#myCarouselMB1 .carousel-inner").append(
                            "<div class=\"carousel-item item\">" + pax() + "</div>"
                        );

                        //add state active the first item
                        if(k == 0){
                            $("#myCarouselB1 .carousel-indicators li").addClass("active");
                            $("#myCarouselB1 .carousel-inner .item").addClass("active");
                            $("#myCarouselMB1 .carousel-indicators li").addClass("active");
                            $("#myCarouselMB1 .carousel-inner .item").addClass("active");
                        }
                        // add paxs
                        function pax(){
                            for(var g = 0 ; g < json[data].destinations.pax.length; g++){
                                if(g == k){
                                    return "<div class=\"quote\">"+json[data].destinations.pax[g].text +"</div><div class=\"author\">"+ json[data].destinations.pax[g].author + "</div>";
                                }
                            }
                        }
                    }

                    // GRAB INBOUND
                    $("#GTB2 .box-content").html(

                        "<div class=\"text\">"+ json[data].destinations.statistics +"</div><br/>"+

                        "<div class=\"text\">"+ json[data].destinations.spending +"</div>"
                    );
                    $("#modalB2 .box-content").html(

                        "<div class=\"text\">"+ json[data].destinations.statistics +"</div><br/>"+

                        "<div class=\"text\">"+ json[data].destinations.spending +"</div>"
                    );

                    // STORY HIGHLIGHTS
                    // Add Videos Slides
                    for( var k = 0; k < json[data].videos.length; k++){

                        // Add indicators
                        if(json[data].videos.length > 0){
                            $("#myCarouselB2 .carousel-indicators").append("<li data-target=\"#myCarouselB2\" data-slide-to=\"" + k + "\"></li>");
                            $("#myCarouselMB2 .carousel-indicators").append("<li data-target=\"#myCarouselMB2\" data-slide-to=\"" + k + "\"></li>");
                        }

                        // Add content slides
                        $("#myCarouselB2 .carousel-inner").append(
                            "<div class=\"carousel-item item \">"+videos()+"</div>"
                        );
                        $("#myCarouselMB2 .carousel-inner").append(
                            "<div class=\"carousel-item item \">"+videos()+"</div>"
                        );

                        // Add state active the first item
                        if(k === 0){
                            $("#myCarouselB2 .carousel-indicators li").addClass("active");
                            $("#myCarouselB2 .carousel-inner .item").addClass("active");
                            $("#myCarouselMB2 .carousel-indicators li").addClass("active");
                            $("#myCarouselMB2 .carousel-inner .item").addClass("active");
                        }

                        // Add videos
                        function videos(){
                            for(var g = 0 ; g < json[data].videos.length; g++){
                                if(g == k){
                                    return "<iframe class=\"youtube\" width=\"328\" height=\"163\" src=\"" + json[data].videos[g].link + "\" frameborder=\"0\" allowfullscreen></iframe><div class=\"text-caption\">"+ json[data].videos[g].name +"</div>";
                                }
                            }
                        }
                    }
                    // Add Images Slides
                    for( var l = 0; l < json[data].images.length; l++){

                        // Add indicators
                        if(json[data].images.length > 0){
                            $("#myCarouselB2 .carousel-indicators").append("<li data-target=\"#myCarouselB2\" data-slide-to=\"" + (k+l) + "\"></li>");
                            $("#myCarouselMB2 .carousel-indicators").append("<li data-target=\"#myCarouselMB2\" data-slide-to=\"" + (k+l) + "\"></li>");
                        }

                        // Add content slides
                        $("#myCarouselB2 .carousel-inner").append(
                            "<div class=\"carousel-item item \">"+images()+"</div>"
                        );
                        $("#myCarouselMB2 .carousel-inner").append(
                            "<div class=\"carousel-item item \">"+images()+"</div>"
                        );


                        // Add state active the first item
                        if(json[data].videos.length === 0 && l === 0){
                            $("#myCarouselB2 .carousel-indicators li").addClass("active");
                            $("#myCarouselB2 .carousel-inner .item").addClass("active");
                            $("#myCarouselMB2 .carousel-indicators li").addClass("active");
                            $("#myCarouselMB2 .carousel-inner .item").addClass("active");
                        }

                        // Add images
                        function images(){
                            for(var g = 0 ; g < json[data].images.length; g++){
                                if(g == l){
                                    return "<a href=\"" + json[data].images[g].link  + " \" target=\"_blank\" ><img src=\"" + json[data].images[g].src  + " \" alt=\"\"><\a><div class=\"text-caption\">" + json[data].images[g].text +"</div>";
                                }
                            }
                        }
                    }


                    // GRAB FACTS
                    // Add slides

                    var fact_length = 0;
                    if (json[data].start != ""){
                        fact_length++;
                    }
                    if (json[data].services != ""){
                        fact_length++;
                    }
                    if (json[data].interest != ""){
                        fact_length++;
                    }

                    for (var k = 0; k < fact_length - 1; k++){
                        if (fact_length > 1) {
                            $("#myCarouselB3 .carousel-indicators").append("<li data-target=\"#myCarouselB3\" data-slide-to=\"" + k + "\"></li>");
                            $("#myCarouselMB3 .carousel-indicators").append("<li data-target=\"#myCarouselMB3\" data-slide-to=\"" + k + "\"></li>");
                        }

                        if ( k == 0){
                            $("#myCarouselB3 .carousel-inner").append(
                                "<div class=\"carousel-item item\">" + "<div class='title'>Year started</div><div class='subtitle'>" + json[data].start + "</div><br/><div class='title'>Services available now</div><div class='subtitle'>" + json[data].services + "</div></div>"
                            );
                            $("#myCarouselMB3 .carousel-inner").append(
                                "<div class=\"carousel-item item\">" + "<div class='title'>Year started</div><div class='subtitle'>" + json[data].start + "</div><br/><div class='title'>Services available now</div><div class='subtitle'>" + json[data].services + "</div></div>"
                            );
                        }

                        if (k == 1){
                            if (json[data].interest.link != "") {
                                $("#myCarouselB3 .carousel-inner").append(
                                    "<div class=\"carousel-item item\">" + "<div class='title'>Interesting Factoid</div><div class='subtitle'>" + "<a href=\"" + json[data].interest.link + " \" target=\"_blank\" >" + json[data].interest.text + "</a></div></div>"
                                );
                                $("#myCarouselMB3 .carousel-inner").append(
                                    "<div class=\"carousel-item item\">" + "<div class='title'>Interesting Factoid</div><div class='subtitle'>" + "<a href=\"" + json[data].interest.link + " \" target=\"_blank\" >" + json[data].interest.text + "</a></div></div>"
                                );
                            }else{
                                $("#myCarouselB3 .carousel-inner").append(
                                    "<div class=\"carousel-item item\">" + "<div class='title'>Interesting Factoid</div><div class='subtitle'>" + json[data].interest.text + "</div></div>"
                                );
                                $("#myCarouselMB3 .carousel-inner").append(
                                    "<div class=\"carousel-item item\">" + "<div class='title'>Interesting Factoid</div><div class='subtitle'>" + json[data].interest.text + "</div></div>"
                                );
                            }
                        }

                        if (k == 0){
                            //add state active the first item
                            $("#myCarouselB3 .carousel-indicators li").addClass("active");
                            $("#myCarouselB3 .carousel-inner .item").addClass("active");
                            $("#myCarouselMB3 .carousel-indicators li").addClass("active");
                            $("#myCarouselMB3 .carousel-inner .item").addClass("active");
                        }
                    }
                }
            }
        };

        //This function is a principal changer dynamic data
        $.changeCountry = (state, iso, isob) =>{
            for( var data in json) {
                //searching Country
                if(json[data].iso === iso){
                    country = iso; //update country active
                    if(state == 'origin'){
                        //State Origin
                        $('#origin').show();
                        $('#nocountry').hide();
                        contentOrigin(state, iso);

                    }else{
                        //State Destination
                        for( var datab in json[data].destinations.countries){
                            if(json[data].destinations.countries[datab] == isob){
                                countryB == isob;
                            }
                        }

                        $('#destination').show();
                        $('#nocountry').hide();
                        contentDestination(state, iso);
                    }
                }
            }
            if(country == "" || iso == ""){
                $('#destination').hide();
                $('#origin').hide();
                $('#nocountry').show();
            }
        };

        $.appearNavBar = () => {
            $('.navbar').show();
        };

        $('.back_home').bind("click", function(event) {
            event.preventDefault();
            window.mySwipe.slide(0, 400);
            for (var i = 0; i <  $('#NavD li a.active').length; i++) {
                $('#NavD li a.active')[i].classList.remove('active');
            }
            for (var i = 0; i <  $('#NavO li a.active').length; i++) {
                $('#NavO li a.active')[i].classList.remove('active');
            }
            $('.logo').show();
        });

        $('#NavO li a').click(function(){
            $('.logo').hide();
            console.log("entro");
            window.mySwipe.slide($(this).data("target"), 400);
            for (var i = 0; i <  $('#NavO li a.active').length; i++) {
                $('#NavO li a.active')[i].classList.remove('active');
            }
            $(this).addClass("active");
        });

        $('#NavD li a').click(function(){
            $('.logo').hide();
            window.mySwipe.slide($(this).data("target"), 400);
            for (var i = 0; i <  $('#NavD li a.active').length; i++) {
                $('#NavD li a.active')[i].classList.remove('active');
            }
            $(this).addClass("active");
        });

        $('.nav-link.tab-o').hover(function() {
            if($("ul li a.tab-d.active").index() == 0){
                $('.o-class').show();
                $('.d-class').hide();
            }
        });

        $('.nav-link.tab-o').mouseleave(function() {
            if($("ul li a.tab-d.active").index() == 0){
                $('.d-class').show();
                $('.o-class').hide();
            }
        });

        $('.nav-link.tab-d').hover(function() {
            if($("ul li a.tab-o.active").index() == 0){
                $('.d-class').show();
                $('.o-class').hide();
            }
        });

        $('.nav-link.tab-d').mouseleave(function() {
            if($("ul li a.tab-o.active").index() == 0){
                $('.o-class').show();
                $('.d-class').hide();
            }
        });


        $('#myTab .tab-d').click(function (e) {
            if (detectmob()){
                $('.logo').show();
                if(state == 'origin'){
                    $("#sliderD").css('visibility', 'visible');
                    $("#sliderO").css('visibility', 'hidden');
                    $(".navbar").hide();
                    for (var i = 0; i <  $('#NavO li a.active').length; i++) {
                        $('#NavO li a.active')[i].classList.remove('active');
                    }

                    setTimeout(function() {
                        window.mySwipe.kill();
                    }, 600);
                    setTimeout(function(){
                            window.mySwipe = new Swipe(document.getElementById('sliderD'), {
                                startSlide: 0,
                                speed: 400,
                                auto: 0,
                                draggable: false,
                                continuous: false,
                                disableScroll: false,
                                stopPropagation: true,
                                callback: function(index, elem, dir) {
                                },
                                transitionEnd: function(index, elem) {

                                }
                            });
                            window.mySwipe.disable();
                        }, 700);

                    if ($('#grab_map.origin')) {
                        $('#sliderO').hide();
                        $('#sliderD').show();
                        $('#grab_map').removeClass('origin');
                        $('#grab_map').addClass('destination');
                        state = 'destination';
                        UnselectCountry();
                        $.changeCountry(state, "", "");
                    }
                    e.preventDefault();
                    $(this).tab('show');

                    onCountry = false;
                }
                if (onCountry && state == 'destination'){
                    window.mySwipe.slide(0, 400);
                    for (var i = 0; i <  $('#NavD li a.active').length; i++) {
                        $('#NavD li a.active')[i].classList.remove('active');
                    }
                }
            }else{
                if ($('#grab_map.origin')) {
                    $('#grab_map').removeClass('origin');
                    $('#grab_map').addClass('destination');
                    $('#sliderO').hide();
                    $('#sliderD').show();
                    state = 'destination';
                    UnselectCountry();
                    $.changeCountry(state, "", "");
                }

                e.preventDefault();
                $(this).tab('show');

                onCountry = false;
            }
        });

        $('#myTab .tab-o').click(function (e) {
            if(detectmob()){
                $('.logo').show();
                if(state == 'destination'){
                    $("#sliderO").css('visibility', 'visible');
                    $("#sliderD").css('visibility', 'hidden');
                    $(".navbar").hide();
                    for (var i = 0; i <  $('#NavD li a.active').length; i++) {
                        $('#NavD li a.active')[i].classList.remove('active');
                    }

                    setTimeout(function() {
                        window.mySwipe.kill();
                    }, 700);
                    setTimeout(function(){
                            window.mySwipe = new Swipe(document.getElementById('sliderO'), {
                                startSlide: 0,
                                speed: 400,
                                auto: 0,
                                draggable: false,
                                continuous: false,
                                disableScroll: false,
                                stopPropagation: true,
                                callback: function(index, elem, dir) {
                                },
                                transitionEnd: function(index, elem) {

                                }
                            });
                            window.mySwipe.disable();
                        },
                        700);

                    if($('#grab_map.destination')){
                        $('#sliderD').hide();
                        $('#sliderO').show();
                        $('#grab_map').removeClass('destination');
                        $('#grab_map').addClass('origin');
                        state = 'origin';
                        UnselectCountry();
                        $.changeCountry(state, "", "");
                    }

                    e.preventDefault();
                    $(this).tab('show');

                    onCountry = false;
                }
                if (onCountry && state == 'origin'){
                    window.mySwipe2.slide(0, 400);
                    for (var i = 0; i <  $('#NavO li a.active').length; i++) {
                        $('#NavO li a.active')[i].classList.remove('active');
                    }
                }
            }else{
                if($('#grab_map.destination')){
                    $('#grab_map').removeClass('destination');
                    $('#grab_map').addClass('origin');
                    $('#sliderD').hide();
                    $('#sliderO').show();
                    state = 'origin';
                    UnselectCountry();
                    $.changeCountry(state, "", "");
                }

                e.preventDefault();
                $(this).tab('show');

                onCountry = false;
            }
        });

        $.changeCountry(state, "", "");
        $("#pickups").hide();
        $("#pickups-1").hide();
    })
})(jQuery);

// Add data For Origin or for Destination
var dt = [];

for(var data in json){
    if(country === json[data].iso){
        if(state === 'origin'){
            dt = json[data].origin.countries;
        }
        else
        {
            dt = json[data].destination.countries;
        }
    }
};

if (!detectmob()) {
    $('#grab_map').addClass("desktop");
    if ($(window).width() > 1700) {
        coords_sing = [
            {"latitude": 1.36666666, "longitude": 103.8},
            {"latitude": -1, "longitude": 85}
        ];
    }else{
        coords_sing = [
            {"latitude": 1.36666666, "longitude": 103.8},
            {"latitude": -2, "longitude": 85}
        ];
    }
}else{
    $('#grab_map').addClass("mobile");
    coords_sing = [
        {"latitude": 1.36666666, "longitude": 103.8},
        {"latitude": -4, "longitude": 95}
    ];
}


//===========================================================================================
//===========================================================================================
//=================================                 =========================================
//=========================             TUTORIAL            =================================

if(detectmob()){
    $('.tutorial_mobile').show();
}else{
    $('.tutorial').show();
}

function showTutorial_Mobile(){
    var tutorialPage = 1;
    if (indicator) {
        $('.tutorial_mobile').css('background-image','url(src/Assets/Tutorial/Mobile/' + tutorialPage + '.jpg' );
    }

    $('.t-skipToMap_mobile').click(function() {
        $('.tutorial_mobile').css('display','none');
    })

    $('.tutorial_mobile').click(function() {
        tutorialPage++;

        //chance background
        if (tutorialPage <= 6) {
            $('.tutorial_mobile').css('background-image', 'url(src/Assets/Tutorial/Mobile/' + tutorialPage + '.jpg');
            if (tutorialPage == 6){
                $('.t-skipToMap_mobile').css('display','none');
            }
        }
        else {
            $('.tutorial_mobile').css('display', 'none');
        }
    })
}

function showTutorial() {

    var tutorialPage = 1;
    if (indicator) {
        $('.tutorial').css('background-image','url(src/Assets/Tutorial/' + tutorialPage + '.jpg' );
        $('.t-text1').css('display','block');
    }

    $('.t-skipToMap').click(function() {
        $('.tutorial').css('display','none');
    })

    $('.tutorial').click(function() {
        tutorialPage++;

        //chance background
        if(tutorialPage <= 6) {
            $('.tutorial').css('background-image','url(src/Assets/Tutorial/' + tutorialPage + '.jpg');
        }
        else {
            $('.t-text6').css('display','none');
            $('.tutorial').css('display','none');
        }

        switch(tutorialPage){
            case 1:
                $('.t-text1').css('display','block');
                break;
            case 2:
                //disable other texts
                $('.t-text1').css('display','none');
                $('.t-text2').css('display','block');
                break;
            case 3:
                $('.t-text1').css('display','none');
                $('.t-text2').css('display','none');
                $('.t-text3').css('display','block');
                break;
            case 4:
                $('.t-text1').css('display','none');
                $('.t-text2').css('display','none');
                $('.t-text3').css('display','none');
                $('.t-text4').css('display','block');
                break;
            case 5:
                $('.t-text1').css('display','none');
                $('.t-text2').css('display','none');
                $('.t-text3').css('display','none');
                $('.t-text4').css('display','none');
                $('.t-text5').css('display','block');
                break;
            case 6:
                $('.t-text1').css('display','none');
                $('.t-text2').css('display','none');
                $('.t-text3').css('display','none');
                $('.t-text4').css('display','none');
                $('.t-text5').css('display','none');
                $('.t-skipToMap').css('display','none');
                $('.t-clickToProceed').css('display','none');
                $('.t-startExplore').css('display','block');
                $('.t-text6').css('display','block');
                break;
            default:
                break;
        }
    })
}


//===========================================================================================
//===========================================================================================
//=================================                 =========================================
//=========================         DESTINATION MAP         =================================


// Create map instance
var chartTwo = am4core.create("chartdiv-2", am4maps.MapChart);
chartTwo.fontFamily = "Sanomat";

// Set map definition
chartTwo.geodata = am4geodata_worldLow;

// Set projection
chartTwo.projection = new am4maps.projections.Miller();
chartTwo.seriesContainer.events.disableType("doublehit");
chartTwo.chartContainer.background.events.disableType("doublehit");
chartTwo.seriesContainer.draggable = false;
chartTwo.seriesContainer.resizable = false;
chartTwo.preloader.disabled = true;
chartTwo.chartContainer.wheelable = false;


if (!detectmob()){
    // Center Point
    chartTwo.homeGeoPoint = {
        "longitude": (originCountries[3].longitude + 9),
        "latitude": (originCountries[3].latitude - 5)
    };
    chartTwo.homeZoomLevel = 1.6;

}else{
    // Center Point
    chartTwo.homeGeoPoint = {
        "longitude": (originCountries[5].longitude - 1),
        "latitude": (originCountries[5].latitude - 15)
    };
    chartTwo.homeZoomLevel = 2.8;
}


chartTwo.events.on("hit", (ev) => {
    if (onCountry && ev.target.className === "MapChart") {
        UnselectCountry();
        onCountry = false;
        if(detectmob()){
            $(".navbar").hide();
        }
    }
});

var lineSingaporeDestination = chartTwo.series.push(new am4maps.MapLineSeries());
lineSingaporeDestination.interactionsEnabled = false;
lineSingaporeDestination.mapLines.template.shortestDistance = false; // keep the lines straight
lineSingaporeDestination.mapLines.template.visible = true;
lineSingaporeDestination.mapLines.template.stroke = am4core.color("#000");
lineSingaporeDestination.mapLines.template.line.strokeWidth = 0.3;
lineSingaporeDestination.zIndex = 1;
lineSingaporeDestination.data = [{
    "multiGeoLine": [
        coords_sing
    ]
}];


// Normal Map Destination
var polygonSeriesTwo = chartTwo.series.push(new am4maps.MapPolygonSeries());
polygonSeriesTwo.useGeodata = true;
polygonSeriesTwo.include = ["AM", "AZ", "BH", "BD", "BT", "CN", "GE", "HK", "IN", "IR", "IQ", "IL", "JP", "JO", "KZ", "KW", "KG", "LB", "MO", "MV", "MN", "NP", "KP", "OM", "PK", "PS", "QA", "SA", "KR", "LK", "SY", "TW", "TJ", "TR", "TM", "AE", "UZ", "YE", "KH", "LA", "SG", "PH"];

// Clickable Map Destination
var polygonSeries2Two = chartTwo.series.push(new am4maps.MapPolygonSeries());
polygonSeries2Two.useGeodata = true;
polygonSeries2Two.include = ["ID", "MY", "MM", "SG", "TH", "VN", "PH"];
polygonSeries2Two.tooltip.label.interactionsEnabled = true;
polygonSeries2Two.tooltip.pointerOrientation = "vertical";




// Template Normal Map Destination
var polygonTemplateTwo = polygonSeriesTwo.mapPolygons.template;
polygonTemplateTwo.stroke = am4core.color("rgba(255,255,255,0)");
polygonTemplateTwo.fill = am4core.color("#EAEAEA");


// Template Clickable Map Destination
var polygonTemplate2Two = polygonSeries2Two.mapPolygons.template;
polygonTemplate2Two.stroke = am4core.color("rgba(255,255,255,1)");
polygonTemplate2Two.fill = am4core.color("#4EC483");
polygonTemplate2Two.tooltipText = "";
polygonTemplate2Two.data = JSON.parse(JSON.stringify(originCountries));
polygonTemplate2Two.cursorOverStyle = [
    // change the cursor on hover to make it apparent the object can be interacted with
    {
        "property": "cursor",
        "value": "pointer"

    }
];
polygonTemplate2Two.events.on("hit", (ev) => {
    onCountry = true;
    onCountryClick(ev);
});

if (!detectmob()) {
    polygonTemplate2Two.tooltipText = "{name}";
    polygonTemplate2Two.events.on("over", (ev) => {
        if (!onCountry) {
            ev.target.fill = am4core.color("#008B3E");
            ev.target.tooltipText = "{name}";
        }

        if (onCountry && ev.target.dataItem.dataContext.id !== selectedCountry) {
            $("#pickups").show();
            ev.target.fill = am4core.color("#52C6D8");
            ev.target.opacity = 0.8;
            ev.target.tooltipText = "";
            ShowInformation_country(ev.target.dataItem.dataContext.id);

            for (let element of pieSeries2Two.slices) {
                if (element.dataItem.dataContext.isoB == ev.target.dataItem.dataContext.id) {
                    element.isActive = true;
                }
            }

            for (var data in json) {
                if (json[data].iso === ev.target.dataItem.dataContext.id) {
                    left = json[data].x_pickups;
                    top = json[data].y_pickups;
                }
            }

            if (selectedCountry === "SG") {
                if (ev.target.dataItem.dataContext.id === "ID") {
                    left -= 180;
                    top += 63;
                }

                if (ev.target.dataItem.dataContext.id === "MY") {
                    left -= -10;
                    top += 200;
                }
                if (ev.target.dataItem.dataContext.id === "TH") {
                    left -= 150;
                    top += 200;
                }
            }

            if (selectedCountry === "VN") {
                if (ev.target.dataItem.dataContext.id === "TH") {
                    left -= -150;
                    top += 0;
                }
            }


            $(document).on('mousemove', function (e) {
                if ($("#chartdiv-2").width() == 1150 & e.pageX < 800){
                    $('#pickups').css({
                        left: e.pageX ,
                        top: e.pageY - top
                    });
                }else{
                    $('#pickups').css({
                        left: e.pageX - left,
                        top: e.pageY - top
                    });
                }

            });

            if (onCountry) {
                zoomToPlace();
                drawLines(selectedCountry, ev.target.dataItem.dataContext.id);
            }
        }
    });

    polygonTemplate2Two.events.on("out", (ev) => {
        if (!onCountry) {
            ev.target.fill = am4core.color("#4EC483");
        }

        if (onCountry && ev.target.dataItem.dataContext.id !== selectedCountry) {
            $("#info-chart").hide();
            $("#pickups").hide();
            ev.target.fill = am4core.color("#D8D8D8");

            //change inactive pieSerieItem
            for (let element of pieSeries2Two.slices) {
                element.isActive = false;
            }

            zoomToPlace();
            drawLines(selectedCountry, false);
        }
    });
}


// Mini Map for Singapore Destinations Tab
var chartMini2 = am4core.create("chartdivMini-2", am4maps.MapChart);
// Set map definition
chartMini2.geodata = am4geodata_worldLow;
// Set projection
chartMini2.projection = new am4maps.projections.Miller();

chartMini2.homeGeoPoint = {
    latitude: singapore.latitude,
    longitude: singapore.longitude
};
chartMini2.homeZoomLevel = 27;
chartMini2.seriesContainer.draggable = false;
chartMini2.seriesContainer.resizable = false;
chartMini2.seriesContainer.events.disableType("doublehit");
chartMini2.chartContainer.background.events.disableType("doublehit");
chartMini2.chartContainer.wheelable = false;

// Normal Mini Map Destination Tab
var polygonSeriesMiniTwo = chartMini2.series.push(new am4maps.MapPolygonSeries());
polygonSeriesMiniTwo.useGeodata = true;
polygonSeriesMiniTwo.include = ["SG", "MY"];

var polygonTemplateMiniTwo = polygonSeriesMiniTwo.mapPolygons.template;
polygonTemplateMiniTwo.stroke = am4core.color("#FFFFFF");
polygonTemplateMiniTwo.fill = am4core.color("#EAEAEA");

// Clickable Mini Map Destination Tab
var polygonSeriesMini2Two = chartMini2.series.push(new am4maps.MapPolygonSeries());
polygonSeriesMini2Two.useGeodata = true;
polygonSeriesMini2Two.include = ["SG"];
polygonSeriesMini2Two.tooltip.label.interactionsEnabled = true;
polygonSeriesMini2Two.tooltip.pointerOrientation = "vertical";

var polygonTemplateMini2Two = polygonSeriesMini2Two.mapPolygons.template;
polygonTemplateMini2Two.stroke = am4core.color("#FFFFFF");
polygonTemplateMini2Two.fill = am4core.color("#4EC483");
polygonTemplateMini2Two.tooltipText = "";
polygonTemplateMini2Two.data = JSON.parse(JSON.stringify(originCountries));
polygonTemplateMini2Two.cursorOverStyle = [
    // change the cursor on hover to make it apparent the object can be interacted with
    {
        "property": "cursor",
        "value": "pointer"

    }
];
polygonTemplateMini2Two.events.on("hit", (ev) => {
    onCountry = true;
    onCountryClick(ev);
});

if (!detectmob()) {
    polygonTemplateMini2Two.tooltipText = "{name}";
    polygonTemplateMini2Two.events.on("over", (ev) => {
        if (!onCountry) {
            ev.target.fill = am4core.color("#008B3E");
            ev.target.tooltipText = "{name}";
            $("#info-chart").hide();
            $("#pickups").hide();
        }
        if (onCountry && ev.target.dataItem._dataContext.id !== selectedCountry) {
            $("#pickups").show();
            ev.target.tooltipText = "";
            ev.target.fill = am4core.color("#52C6D8");
            ShowInformation_country(ev.target.dataItem.dataContext.id);

            for (let element of pieSeries2Two.slices) {
                if (element.dataItem.dataContext.isoB == ev.target.dataItem.dataContext.id) {
                    element.isActive = true;
                }
            }

            for (var data in json) {
                if (json[data].iso === ev.target.dataItem.dataContext.id) {
                    left = json[data].x_pickups;
                    top = json[data].y_pickups;

                }
            }

            $(document).on('mousemove', function (e) {
                $('#pickups').css({
                    left: e.pageX - left,
                    top: e.pageY - top
                });
            });

            zoomToPlace();
            drawLines(selectedCountry, ev.target.dataItem.dataContext.id);
        }
    });

    polygonTemplateMini2Two.events.on("out", (ev) => {
        if (!onCountry) {
            ev.target.fill = am4core.color("#4EC483");
        }

        if (onCountry && ev.target.dataItem._dataContext.id !== selectedCountry) {
            ev.target.fill = am4core.color("#D8D8D8");
            $("#info-chart").hide();
            $("#pickups").hide();

            //change inactive pieSerieItem
            for (let element of pieSeries2Two.slices) {
                element.isActive = false;
            }

            if (onCountry) {
                zoomToPlace();
                drawLines(selectedCountry, false);
            }
        }
    });
}

// For put the point and drawlines.
var dimageSeriesTwo = chartTwo.series.push(new am4maps.MapImageSeries());
    dimageSeriesTwo.zIndex = 5;

var dImageSeriesTemplateTwo = dimageSeriesTwo.mapImages.template;
    dImageSeriesTemplateTwo.horizontalCenter = "middle";
    dImageSeriesTemplateTwo.verticalCenter = "middle";
    dImageSeriesTemplateTwo.align = "center";
    dImageSeriesTemplateTwo.valign = "middle";
    dImageSeriesTemplateTwo.nonScaling = true;
    dImageSeriesTemplateTwo.tooltipText = "";
    dImageSeriesTemplateTwo.fill = am4core.color("#fff");
    dImageSeriesTemplateTwo.background.fillOpacity = 0;
    dImageSeriesTemplateTwo.background.fill = "#fff";
    dImageSeriesTemplateTwo.propertyFields.id = "id";
    dImageSeriesTemplateTwo.propertyFields.latitude = "latitude";
    dImageSeriesTemplateTwo.propertyFields.longitude = "longitude";
    dImageSeriesTemplateTwo.interactionsEnabled = false;

var dLabelTwo = dImageSeriesTemplateTwo.createChild(am4core.Label);
    dLabelTwo.text = "{title}";
    dLabelTwo.horizontalCenter = "middle";
    dLabelTwo.verticalCenter = "top";
    dLabelTwo.dy = 10;
    dLabelTwo.fontSize = 10;
    dLabelTwo.interactionsEnabled = false;
    dLabelTwo.zIndex = 5;
    // Add white box behind labels
    dLabelTwo.fill = am4core.color("#000");
    dLabelTwo.background.fill = am4core.color("#fff");
    dLabelTwo.paddingBottom = 3;
    dLabelTwo.paddingLeft = 3;
    dLabelTwo.paddingRight = 3;
    dLabelTwo.paddingTop = 3;


// ========== From Here is the Pie Chart Interaction Destination ===================
var chart2Two = am4core.create("piechart-2", am4charts.PieChart);
chart2Two.data = dt;
// Add label
chart2Two.innerRadius = am4core.percent(60);
if (detectmob()) {
    chart2Two.innerRadius = 40;
}

var circle2Two = chart2Two.createChild(am4core.Circle);
circle2Two.fill = am4core.color("#4EC483");
circle2Two.stroke = am4core.color("#FFF");
circle2Two.height = am4core.percent(50);
circle2Two.width = am4core.percent(50);
if (detectmob()) {
    circle2Two.height = am4core.percent(48);
    circle2Two.width = am4core.percent(48);
}
circle2Two.horizontalCenter = "middle";
circle2Two.verticalCenter = "middle";
circle2Two.x = am4core.percent(50);
circle2Two.y = am4core.percent(50);
circle2Two.align = "center";
circle2Two.isMeasured = false;
circle2Two.toBack();

var label2Two = chart2Two.seriesContainer.createChild(am4core.Label);
label2Two.text = countryName;
label2Two.fill = am4core.color("#fff");
label2Two.background = am4core.Circle;
label2Two.horizontalCenter = "middle";
label2Two.verticalCenter = "middle";
label2Two.dy = 0;
label2Two.dx = 0;
label2Two.alignLabels = "center";
label2Two.fontSize = 16;
if (detectmob()) {
    label2Two.fontSize = 11;
}

// Add and configure Series
var pieSeries2Two = chart2Two.series.push(new am4charts.PieSeries());
pieSeries2Two.dataFields.value = "size";
pieSeries2Two.dataFields.category = "isoB";
// Disable ticks and labels
pieSeries2Two.labels.template.disabled = true;
pieSeries2Two.ticks.template.disabled = true;
// Disable tooltips
pieSeries2Two.slices.template.tooltipText = "";
if (!detectmob()) {
    pieSeries2Two.slices.template.events.on("out", (ev) => {
        for (let element of pieSeries2Two.slices) {
            element.isActive = false;
        }
        for (let element of pieSeries2Two.slices) {
            if (!element.isActive) {
                colorDeselectedCountry(element.dataItem.dataContext.isoB);
            }
        }
        $("#info-chart").hide();
        zoomToPlace();
        drawLines(selectedCountry, false);
    });
    pieSeries2Two.slices.template.events.on("over", (ev) => {
        $("#pickups").hide();
        colorSelectedCountry(ev.target.dataItem.dataContext.isoB);
        for (let element of pieSeries2Two.slices) {
            if (!element.isActive && element.dataItem.dataContext.isoB !== ev.target.dataItem.dataContext.isoB) {
                colorDeselectedCountry(element.dataItem.dataContext.isoB);
            }
        }

        for (var data in json) {
            if (json[data].iso === selectedCountry) {
                left_info = json[data].x_info;
                top_info = json[data].y_info;
            }
        }

        $(document).on('mousemove', function (e) {
            $('#info-chart').css({
                left: e.pageX - left_info,
                top: e.pageY - top_info
            });
        });

        $("#info-chart").show();
        ShowInformation_pie(ev.target.dataItem.dataContext.isoB);

        //Draw one line
        zoomToPlace();
        drawLines(selectedCountry, ev.target.dataItem.dataContext.isoB);

    });
} else {
    pieSeries2Two.slices.template.events.on("hit", (ev) => {
        colorSelectedCountry(ev.target.dataItem.dataContext.isoB);
        for (let element of pieSeries2Two.slices) {
            element.isActive = false;
            if (!element.isActive && element.dataItem.dataContext.isoB !== ev.target.dataItem.dataContext.isoB) {
                colorDeselectedCountry(element.dataItem.dataContext.isoB);
            }
        }

        for (var data in json) {
            if (json[data].iso === selectedCountry) {
                left = json[data].x_info_mobile;
                top = json[data].y_info_mobile;
            }
        }
        if (detectmob()) {
            $('#pickups').removeClass();
            $('#info-chart').removeClass();
            $('#pickups').addClass(selectedCountry);
            $('#info-chart').addClass(selectedCountry);
        }

        $('#info-chart').css({
            left: left,
            top: top
        });

        $("#info-chart").show();
        ShowInformation_pie(ev.target.dataItem.dataContext.isoB)

        for (var data in json) {
            if (json[data].iso === selectedCountry) {
                left = json[data].x_pickups_mobile;
                top = json[data].y_pickups_mobile;
            }
        }

        $('#pickups').css({
            left: left,
            top: top
        });

        $("#pickups").show();
        ShowInformation_country(ev.target.dataItem.dataContext.isoB);

        //Draw one line
        zoomToPlace();
        drawLines(selectedCountry, ev.target.dataItem.dataContext.isoB);
    });
}

pieSeries2Two.colors.list = [
    am4core.color("#bfbfbf"),
    am4core.color("#989898"),
    am4core.color("#717171")
];
// Put a thick white border around each Slice
pieSeries2Two.slices.template.stroke = am4core.color("#fff");
pieSeries2Two.slices.template.strokeWidth = 1;
pieSeries2Two.slices.template.strokeOpacity = 1;


function deselectAllPieDestination() {
    for (let element of pieSeries2Two.slices) {
        if (element.isActive !== true) {
            element.isActive = false;
        }
    }
}

// Create hover state
var hoverState2 = pieSeries2Two.slices.template.states.getKey("hover");

// Activate State Pie Series 2 -- Fix this comment in the future
var activeState2 = pieSeries2Two.slices.template.states.getKey("active");
activeState2.properties.scale = 1;
activeState2.properties.fillOpacity = 1;



//===========================================================================================
//===========================================================================================
//=================================                 =========================================
//=========================         ORIGIN MAP         =================================


// Create map instance
var chart = am4core.create("chartdiv-1", am4maps.MapChart);
// Set map definition
chart.geodata = am4geodata_worldLow;
chart.fontFamily = "Sanomat";
// Set projection
chart.projection = new am4maps.projections.Miller();
chart.seriesContainer.events.disableType("doublehit");
chart.chartContainer.background.events.disableType("doublehit");

// Zoom control
chart.seriesContainer.draggable = false;
chart.seriesContainer.resizable = false;

if (!detectmob()){
    // Center Point
    chart.homeGeoPoint = {
        "longitude": (originCountries[3].longitude + 9),
        "latitude": (originCountries[3].latitude - 5)
    };
    chart.homeZoomLevel = 1.6;

}else{
    // Center Point
    chart.homeGeoPoint = {
        "longitude": (originCountries[5].longitude - 1),
        "latitude": (originCountries[5].latitude - 15)
    };
    chart.homeZoomLevel = 2.8;
}

chart.preloader.disabled = true;
chart.chartContainer.wheelable = false;
chart.events.on("hit", (ev) => {
    if (onCountry && ev.target.className === "MapChart") {
        UnselectCountry();
        onCountry = false;
        if(detectmob()){
            $(".logo").show();
            $(".navbar").hide();
        }
    };
});

var lineSingaporeOrigin = chart.series.push(new am4maps.MapLineSeries());

// Normal Map
var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());
polygonSeries.useGeodata = true;
polygonSeries.include = ["AM", "AZ", "BH", "BD", "BT", "CN", "GE", "HK", "IN", "IR", "IQ", "IL", "JP", "JO", "KZ", "KW", "KG", "LB", "MO", "MV", "MN", "NP", "KP", "OM", "PK", "PS", "QA", "SA", "KR", "LK", "SY", "TW", "TJ", "TR", "TM", "AE", "UZ", "YE", "KH", "LA", "SG", "PH"];

// Clickeable Map
var polygonSeries2 = chart.series.push(new am4maps.MapPolygonSeries());
polygonSeries2.useGeodata = true;
polygonSeries2.include = ["ID", "MY", "MM", "SG", "TH", "VN", "PH"];
polygonSeries2.tooltip.label.interactionsEnabled = true;
polygonSeries2.tooltip.pointerOrientation = "vertical";


// Template Normal Map
var polygonTemplate = polygonSeries.mapPolygons.template;
polygonTemplate.stroke = am4core.color("rgba(0,0,0,0)");
polygonTemplate.fill = am4core.color("#414652");

// Template Clickeable Map
var polygonTemplate2 = polygonSeries2.mapPolygons.template;
polygonTemplate2.stroke = am4core.color("rgba(0,0,0,1)");
polygonTemplate2.strokeOpacity = 0.8;
polygonTemplate2.fill = am4core.color("#52C6D8");
polygonTemplate2.tooltipText = "";
polygonTemplate2.data = JSON.parse(JSON.stringify(originCountries));
polygonTemplate2.cursorOverStyle = [
    // change the cursor on hover to make it apparent the object can be interacted with
    {
        "property": "cursor",
        "value": "pointer"
    }
];
polygonTemplate2.events.on("hit", (ev) => {
    onCountry = true;
    onCountryClick(ev);
});
if (!detectmob()) {
    polygonTemplate2.tooltipText = "{name}";
    polygonTemplate2.events.on("over", (ev) => {

        if (!onCountry) {
            ev.target.fill = am4core.color("#0F8EA2");
            ev.target.tooltipText = "{name}";
        }

        if (onCountry && ev.target.dataItem._dataContext.id !== selectedCountry) {
            $("#pickups-1").show();
            ev.target.fill = am4core.color("#4EC483");
            ev.target.opacity = 0.8;
            ev.target.tooltipText = "";
            ShowInformation_country(ev.target.dataItem.dataContext.id);

            for (let element of pieSeries2.slices) {
                if (element.dataItem.dataContext.isoB == ev.target.dataItem.dataContext.id) {
                    element.isActive = true;
                }
            }

            for (var data in json) {
                if (json[data].iso === ev.target.dataItem.dataContext.id) {
                    left = json[data].x_pickups;
                    top = json[data].y_pickups;
                }
            }

            if (selectedCountry === "SG") {
                if (ev.target.dataItem.dataContext.id === "ID") {
                    left -= 180;
                    top += 80;
                }

                if (ev.target.dataItem.dataContext.id === "MY") {
                    left -= -10;
                    top += 200;
                }
                if (ev.target.dataItem.dataContext.id === "TH") {
                    left -= 150;
                    top += 200;
                }
            }

            if (selectedCountry === "VN") {
                if (ev.target.dataItem.dataContext.id === "TH") {
                    left -= -150;
                    top += 0;
                }
            }


            $(document).on('mousemove', function (e) {
                $('#pickups-1').css({
                    left: e.pageX - left,
                    top: e.pageY - top
                });
            });

            if (onCountry) {
                zoomToPlace();
                drawLines(selectedCountry, ev.target.dataItem.dataContext.id);
            }

        }
    });

    polygonTemplate2.events.on("out", (ev) => {
        if (!onCountry) {
            ev.target.fill = am4core.color("#52C6D8");
        }

        if (onCountry && ev.target.dataItem.dataContext.id !== selectedCountry) {
            $("#info-chart-1").hide();
            $("#pickups-1").hide();
            ev.target.fill = am4core.color("#5A5F6A");

            //change inactive pieSerieItem
            for (let element of pieSeries2.slices) {
                element.isActive = false;
            }

            zoomToPlace();
            drawLines(selectedCountry, false);
        }
    });
}


//======= Mini Map for Singapore Origin Tab =========
var chartMini = am4core.create("chartdivMini-1", am4maps.MapChart);
// Set map definition
chartMini.geodata = am4geodata_worldLow;
// Set projection
chartMini.projection = new am4maps.projections.Miller();

chartMini.homeGeoPoint = {
    latitude: singapore.latitude,
    longitude: singapore.longitude
};
chartMini.homeZoomLevel = 27;
chartMini.seriesContainer.draggable = false;
chartMini.seriesContainer.resizable = false;
chartMini.seriesContainer.events.disableType("doublehit");
chartMini.chartContainer.background.events.disableType("doublehit");
chartMini.chartContainer.wheelable = false;

// Normal Mini Map Origin
var polygonSeriesMini = chartMini.series.push(new am4maps.MapPolygonSeries());
polygonSeriesMini.useGeodata = true;
polygonSeriesMini.include = ["SG", "MY"];

var polygonTemplateMini = polygonSeriesMini.mapPolygons.template;
polygonTemplateMini.stroke = am4core.color("#363A45");
polygonTemplateMini.fill = am4core.color("#414652");


// Clickable Mini Map Origin
var polygonSeriesMini2 = chartMini.series.push(new am4maps.MapPolygonSeries());
polygonSeriesMini2.useGeodata = true;
polygonSeriesMini2.include = ["SG"];
polygonSeriesMini2.tooltip.label.interactionsEnabled = true;
polygonSeriesMini2.tooltip.pointerOrientation = "vertical";

// Template Mini Map Origin
var polygonTemplateMini2 = polygonSeriesMini2.mapPolygons.template;
polygonTemplateMini2.stroke = am4core.color("#363A45");
polygonTemplateMini2.fill = am4core.color("#52C6D8");
polygonTemplateMini2.tooltipText = "";
polygonTemplateMini2.data = JSON.parse(JSON.stringify(originCountries));
polygonTemplateMini2.cursorOverStyle = [
    // change the cursor on hover to make it apparent the object can be interacted with
    {
        "property": "cursor",
        "value": "pointer"

    }
];
polygonTemplateMini2.events.on("hit", (ev) => {
    onCountry = true;
    onCountryClick(ev);
});

if (!detectmob()) {
    polygonTemplateMini2.events.on("over", (ev) => {
        if (!onCountry) {
            ev.target.fill = am4core.color("#0F8EA2");
            ev.target.tooltipText = "{name}";
            $("#info-chart-1").hide();
            $("#pickups-1").hide();
        }
        if (onCountry && ev.target.dataItem._dataContext.id !== selectedCountry) {
            $("#pickups-1").show();
            ev.target.tooltipText = "";
            ev.target.fill = am4core.color("#4EC483");
            ShowInformation_country(ev.target.dataItem.dataContext.id);

            for (var data in json) {
                if (json[data].iso === ev.target.dataItem.dataContext.id) {
                    left = json[data].x_pickups;
                    top = json[data].y_pickups;
                }
            }

            $(document).on('mousemove', function (e) {
                $('#pickups-1').css({
                    left: e.pageX - left,
                    top: e.pageY - top
                });
            });

            zoomToPlace();
            drawLines(selectedCountry, ev.target.dataItem.dataContext.id);

            for (let element of pieSeries2.slices) {
                if (element.dataItem.dataContext.isoB == ev.target.dataItem.dataContext.id) {
                    element.isActive = true;
                }
            }
        }
    });
    polygonTemplateMini2.events.on("out", (ev) => {
        if (!onCountry) {
            ev.target.fill = am4core.color("#52C6D8");
        }

        if (onCountry && ev.target.dataItem._dataContext.id !== selectedCountry) {
            ev.target.fill = am4core.color("#5A5F6A");
            $("#info-chart-1").hide();
            $("#pickups-1").hide();

            //change inactive pieSerieItem
            for (let element of pieSeries2.slices) {
                element.isActive = false;
            }

            if (onCountry) {
                zoomToPlace();
                drawLines(selectedCountry, false);
            }
        }
    });
}

// Destinations config point
var dimageSeries = chart.series.push(new am4maps.MapImageSeries());
    dimageSeries.zIndex = 5;

var dImageSeriesTemplate = dimageSeries.mapImages.template;
dImageSeriesTemplate.horizontalCenter = "middle";
dImageSeriesTemplate.verticalCenter = "middle";
dImageSeriesTemplate.align = "center";
dImageSeriesTemplate.valign = "middle";
dImageSeriesTemplate.nonScaling = true;
dImageSeriesTemplate.tooltipText = "";
dImageSeriesTemplate.fill = am4core.color("#fff");
dImageSeriesTemplate.background.fillOpacity = 0;
dImageSeriesTemplate.background.fill = "#fff";
dImageSeriesTemplate.propertyFields.id = "id";
dImageSeriesTemplate.propertyFields.latitude = "latitude";
dImageSeriesTemplate.propertyFields.longitude = "longitude";
dImageSeriesTemplate.interactionsEnabled = false;

// Drawing point sprites
var dLabel = dImageSeriesTemplate.createChild(am4core.Label);
dLabel.text = "{title}";
dLabel.horizontalCenter = "middle";
dLabel.verticalCenter = "top";
dLabel.dy = 10;
dLabel.fontSize = 10;
dLabel.interactionsEnabled = false;
dLabel.zIndex = 5;
dLabel.fill = am4core.color("#fff");



// ========== From Here is the Pie Chart Interaction Origin  ===================

var chart2 = am4core.create("piechart-1", am4charts.PieChart);
chart2.data = dt;
// Add label
chart2.innerRadius = am4core.percent(60);
if (detectmob()) {
    chart2.innerRadius = 40;
}

var circle2 = chart2.createChild(am4core.Circle);
circle2.fill = am4core.color("#2096f3");
circle2.stroke = am4core.color("#FFF");
circle2.height = am4core.percent(50);
circle2.width = am4core.percent(50);
if (detectmob()) {
    circle2.height = am4core.percent(48);
    circle2.width = am4core.percent(48);
}
circle2.horizontalCenter = "middle";
circle2.verticalCenter = "middle";
circle2.x = am4core.percent(50);
circle2.y = am4core.percent(50);
circle2.align = "center";
circle2.isMeasured = false;
circle2.toBack();


var label2 = chart2.seriesContainer.createChild(am4core.Label);
label2.text = countryName;
label2.fill = am4core.color("#fff");
label2.background = am4core.Circle;
label2.horizontalCenter = "middle";
label2.verticalCenter = "middle";
label2.dy = 0;
label2.dx = 0;
label2.alignLabels = "center";
label2.fontSize = 16;
if (detectmob()) {
    label2.fontSize = 11;
}

// Add and configure Series
var pieSeries2 = chart2.series.push(new am4charts.PieSeries());
pieSeries2.dataFields.value = "size";
pieSeries2.dataFields.category = "isoB";
// Disable ticks and labels
pieSeries2.labels.template.disabled = true;
pieSeries2.ticks.template.disabled = true;
// Disable tooltips
pieSeries2.slices.template.tooltipText = "";

if (!detectmob()) {
    pieSeries2.slices.template.events.on("out", (ev) => {
        for (let element of pieSeries2.slices) {
            element.isActive = false;
        }
        for (let element of pieSeries2.slices) {
            if (!element.isActive) {
                colorDeselectedCountry(element.dataItem.dataContext.isoB);
            }
        }

        $("#info-chart-1").hide();
        zoomToPlace();
        drawLines(selectedCountry, false);
    });

    pieSeries2.slices.template.events.on("over", (ev) => {
        $("#pickups-1").hide();
        colorSelectedCountry(ev.target.dataItem.dataContext.isoB);
        for (let element of pieSeries2.slices) {
            if (!element.isActive && element.dataItem.dataContext.isoB !== ev.target.dataItem.dataContext.isoB) {
                colorDeselectedCountry(element.dataItem.dataContext.isoB);
            }
        }

        for (var data in json) {
            if (json[data].iso === selectedCountry) {
                left_info = json[data].x_info;
                top_info = json[data].y_info;
            }
        }

        $(document).on('mousemove', function (e) {
            $('#info-chart-1').css({
                left: e.pageX - left_info,
                top: e.pageY - top_info
            });
        });

        $("#info-chart-1").show();
        ShowInformation_pie(ev.target.dataItem.dataContext.isoB);

        //Draw one line
        zoomToPlace();
        drawLines(selectedCountry, ev.target.dataItem.dataContext.isoB);
    });

} else {
    pieSeries2.slices.template.events.on("hit", (ev) => {
        colorSelectedCountry(ev.target.dataItem.dataContext.isoB);
        for (let element of pieSeries2.slices) {
            element.isActive = false;
            if (!element.isActive && element.dataItem.dataContext.isoB !== ev.target.dataItem.dataContext.isoB) {
                colorDeselectedCountry(element.dataItem.dataContext.isoB);
            }
        }

        for (var data in json) {
            if (json[data].iso === selectedCountry) {
                left = json[data].x_info_mobile;
                top = json[data].y_info_mobile;
            }
        }

        if (detectmob()) {
            $('#pickups-1').removeClass();
            $('#info-chart-1').removeClass();
            $('#pickups-1').addClass(selectedCountry);
            $('#info-chart-1').addClass(selectedCountry);
        }

        $('#info-chart-1').css({
            left: left,
            top: top
        });

        $("#info-chart-1").show();
        ShowInformation_pie(ev.target.dataItem.dataContext.isoB)

        for (var data in json) {
            if (json[data].iso === selectedCountry) {
                left = json[data].x_pickups_mobile;
                top = json[data].y_pickups_mobile;
            }
        }

        $('#pickups-1').css({
            left: left,
            top: top
        });

        $("#pickups-1").show();
        ShowInformation_country(ev.target.dataItem.dataContext.isoB);

        //Draw one line
        zoomToPlace();
        drawLines(selectedCountry, ev.target.dataItem.dataContext.isoB);
    });
}

pieSeries2.colors.list = [
    am4core.color("#bfbfbf"),
    am4core.color("#989898"),
    am4core.color("#717171")
];


// Put a thick white border around each Slice
pieSeries2.slices.template.stroke = am4core.color("#fff");
pieSeries2.slices.template.strokeWidth = 1;
pieSeries2.slices.template.strokeOpacity = 1;

function deselectAllPieOrigin() {
    for (let element of pieSeries2.slices) {
        if (element.isActive !== true) {
            element.isActive = false;
        }
    }
}


// Create hover state
var hoverState = pieSeries2.slices.template.states.getKey("hover");

// Activate State Pie Series 2 -- Fix this comment in the future
var activeState = pieSeries2.slices.template.states.getKey("active");
activeState.properties.scale = 1;
activeState.properties.fillOpacity = 1;













//=============================================================================
//=============================================================================
//=============================================================================
//======================= GLOBAL FUNCTIONS  ===================================
//=============================================================================

// LOADCHART Origin
var indicator;
var indicatorInterval;

function showIndicator() {

    if (!indicator) {
        indicator = chart.tooltipContainer.createChild(am4core.Container);
        indicator.background.fill = am4core.color("#fff");
        indicator.background.fillOpacity = 0.8;
        indicator.width = am4core.percent(100);
        indicator.height = am4core.percent(100);

        var indicatorLabel = indicator.createChild(am4core.Label);
        indicatorLabel.text = "Loading map...";
        indicatorLabel.align = "center";
        indicatorLabel.valign = "middle";
        indicatorLabel.fontSize = 20;
        indicatorLabel.dy = 50;

        if(!detectmob()){
            showTutorial();
        }else{
            showTutorial_Mobile();
        }
    }

    indicator.hide(0);
    indicator.show();

    chart.events.on("ready", function(ev){
        hideIndicator();
    });

    function hideIndicator() {
        indicator.hide();
        clearInterval(indicatorInterval);
    }
}
showIndicator();

// LOADCHART Destination
var indicatorTwo;
var indicatorIntervalTwo;

function showIndicatorDestination() {
    if (!indicatorTwo) {
        indicatorTwo = chartTwo.tooltipContainer.createChild(am4core.Container);
        indicatorTwo.background.fill = am4core.color("#fff");
        //indicatorTwo.background.fillOpacity = 0.8;
        indicatorTwo.width = am4core.percent(100);
        indicatorTwo.height = am4core.percent(100);

        var indicatorLabelTwo = indicatorTwo.createChild(am4core.Label);
        indicatorLabelTwo.text = "Loading map...";
        indicatorLabelTwo.align = "center";
        indicatorLabelTwo.valign = "middle";
        indicatorLabelTwo.fontSize = 20;
        indicatorLabelTwo.dy = 50;
    }

    indicatorTwo.hide(0);
    indicatorTwo.show();

    chartTwo.events.on("ready", function(ev){
        hideIndicatorDestination();
    });

    function hideIndicatorDestination() {
        indicatorTwo.hide();
        clearInterval(indicatorIntervalTwo);
    }
}
showIndicatorDestination();

function drawDestinationPoints(countryCode){
    var dest = [];
    for(var i=0;i<originCountries.length;i++){
        if(originCountries[i].id === countryCode){
            dest = originCountries[i].destinations;
        }
    }

    var destList = [];
    dest.forEach(element=>{
        for(var i=0;i<destinationCountries.length;i++){
            if(destinationCountries[i].id === element){
                destList.push(destinationCountries[i]);
            }
        }
    });

    if (state == "destination"){
        dimageSeriesTwo.data = JSON.parse(JSON.stringify(destList));
    }else{
        dimageSeries.data = JSON.parse(JSON.stringify(destList));
    }
}


function drawLines(countryCode, one_country){
    for(var i=0; i<chart.series.values.length;i++){
        if(chart.series.values[i].className === "MapLineSeries"){
            chart.series.removeIndex(i).dispose();
            i=0;
        }
    }
    for(var i=0; i<chartTwo.series.values.length;i++){
        if(chartTwo.series.values[i].className === "MapLineSeries"){
            chartTwo.series.removeIndex(i).dispose();
            i=0;
        }
    }
    var origin = getOriginCountry(countryCode);
    var destinations = getDestinationList(countryCode);
    if (one_country){
        destinations = [getDestinationCountry(one_country)];
    }
    destinations.forEach(dest=>{
        if(dest){
            if ( state == 'origin'){
                lineSingaporeOrigin = chart.series.push(new am4maps.MapLineSeries());
                lineSingaporeOrigin.mapLines.template.stroke = am4core.color("#FFF");
                lineSingaporeOrigin.mapLines.template.strokeWidth = 0.1;
                lineSingaporeOrigin.zIndex = 1;
                lineSingaporeOrigin.data = [{
                    "multiGeoLine": [
                        coords_sing
                    ]
                }];
            }else{
                lineSingaporeDestination = chartTwo.series.push(new am4maps.MapLineSeries());
                lineSingaporeDestination.mapLines.template.stroke = am4core.color("#000");
                lineSingaporeDestination.mapLines.template.strokeWidth = 0.1;
                lineSingaporeDestination.zIndex = 1;
                lineSingaporeDestination.data = [{
                    "multiGeoLine": [
                        coords_sing
                    ]
                }];

            }

            var coords = [
                { "latitude": origin.latitude, "longitude": origin.longitude },
                { "latitude": dest.latitude, "longitude": dest.longitude}
            ];


            // ORIGIN MAP
            if(state == 'origin'){
                var lineSeries = chart.series.push(new am4maps.MapLineSeries());
                lineSeries.interactionsEnabled = false;
                lineSeries.mapLines.template.shortestDistance = false; // keep the lines straight
                lineSeries.zIndex = 0;
                lineSeries.mapLines.template.visible = true;
                if (!detectmob()) {
                    lineSeries.mapLines.template.line.strokeWidth = 1;
                }else{
                    lineSeries.mapLines.template.line.strokeWidth = 1;
                }

                lineSeries.mapLines.template.stroke =  gradientOrigin;

                if (origin.id == "PH" || origin.id == "MY" || origin.id == "ID" || origin.id == "SG" || origin.id == "MY"){
                    lineSeries.mapLines.template.stroke = gradientOriginR;
                }

                var linesOrigin = lineSeries.mapLines.create();
                linesOrigin.multiGeoLine = [
                    coords
                ];




                // Add a map object to line
                var bulletLineO = linesOrigin.lineObjects.create();
                bulletLineO.nonScaling = true;
                bulletLineO.position = 0;
                bulletLineO.horizontalCenter = "middle";
                bulletLineO.verticalCenter = "middle";
                bulletLineO.fill = gradientOrigin;

                var LineO = bulletLineO.createChild(am4core.Sprite);
                LineO.path = "M125 05 L0 0 L0 11";
                LineO.opacity = 0.98;
                LineO.strokeOpacity = 0;


                if (!detectmob()){
                    bulletLineO.width = 70;
                    bulletLineO.height = 32;
                    LineO.scale = 2.8;

                    if (origin.id == "MM"){
                        switch (dest.id){
                            case "VN":
                                LineO.scale = 1.445;
                                bulletLineO.height = 16;
                                break;
                            case "TH":
                                LineO.scale = 1.1;
                                bulletLineO.height = 12;
                                break;
                            case "MY":
                                LineO.scale = 2.2;
                                bulletLineO.height = 24;
                                break;
                            case "SG":
                                LineO.scale = 2.4;
                                bulletLineO.height = 26;
                                break;
                        }
                    }
                    if (origin.id == "VN"){
                        bulletLineO.height = 25;
                        LineO.scale = 2;
                        switch (dest.id){
                            case "TH":
                                LineO.scale = 1.25;
                                bulletLineO.height = 15;
                                break;
                            case "SG":
                                LineO.scale = 1.85;
                                bulletLineO.height = 21;
                                break;
                            case "MY":
                                LineO.scale = 1.65;
                                bulletLineO.height = 21;
                                break;
                            case "MM":
                                LineO.scale = 2.1;
                                //bulletLineO.height = 23;
                                break;
                        }
                    }
                    if (origin.id == "TH"){
                        bulletLineO.height = 22;
                        LineO.scale = 2;
                        switch (dest.id){
                            case "VN":
                                LineO.scale = 1.15;
                                bulletLineO.height = 15;
                                break;
                            case "MM":
                                LineO.scale = 1.65;
                                bulletLineO.height = 20;
                                break;
                            case "MY":
                                LineO.scale = 1.65;
                                bulletLineO.height = 20;
                                break;
                        }
                    }
                    if (origin.id == "PH"){
                        switch (dest.id){
                            case "VN":
                                LineO.scale = 2.4;
                                bulletLineO.height = 28;
                                break;
                            case "ID":
                                LineO.scale = 2.1;
                                bulletLineO.height = 28;
                                break;
                        }
                    }
                    if (origin.id == "MY"){
                        switch (dest.id){
                            case "SG":
                                LineO.scale = 0.7;
                                bulletLineO.height = 15;
                                break;
                            case "ID":
                                LineO.scale = 1.3;
                                bulletLineO.height = 16;
                                break;
                            case "PH":
                                LineO.scale = 1.6;
                                bulletLineO.height = 20;
                                break;
                            case "VN":
                                LineO.scale = 1.6;
                                bulletLineO.height = 18;
                                break;
                            case "TH":
                                LineO.scale = 1.4;
                                bulletLineO.height = 17;
                                break;
                            case "MM":
                                LineO.scale = 2.5;
                                bulletLineO.height = 28;
                                break;
                        }
                    }
                    if (origin.id == "ID"){
                        switch (dest.id){
                            case "SG":
                                LineO.scale = 2.5;
                                bulletLineO.height = 28;
                                break;
                            case "MY":
                                LineO.scale = 2.7;
                                bulletLineO.height = 30;
                                break;
                            case "PH":
                                LineO.scale = 2.3;
                                bulletLineO.height = 26;
                                break;
                            case "VN":
                                LineO.scale = 2.9;
                                bulletLineO.height = 32;
                                break;
                            case "TH":
                                LineO.scale = 3;
                                bulletLineO.height = 32;
                                break;
                            case "MM":
                                LineO.scale = 3.8;
                                bulletLineO.height = 36;
                                break;
                        }
                    }
                    if (origin.id == "SG"){
                        switch (dest.id){
                            case "ID":
                                LineO.scale = 1.6;
                                bulletLineO.height = 20;
                                break;
                            case "MY":
                                LineO.scale = 1;
                                bulletLineO.height = 30;
                                break;
                            case "PH":
                                LineO.scale = 2.7;
                                bulletLineO.height = 29;
                                break;
                            case "VN":
                                LineO.scale = 2.1;
                                bulletLineO.height = 23;
                                break;
                            case "TH":
                                LineO.scale = 1.9;
                                bulletLineO.height = 20;
                                break;
                            case "MM":
                                LineO.scale = 2.7;
                                bulletLineO.height = 28;
                                break;
                        }
                    }
                }else{
                    //bulletLineO.width = 70;
                    bulletLineO.height = 10;
                    //LineO.scale = 2.8;
                    if (origin.id == "MM") {
                        switch (dest.id){
                            case "VN":
                                LineO.scale = .8;
                                break;
                            case "TH":
                                LineO.scale = .5;
                                break;
                        }
                    }
                    if (origin.id == "VN") {
                        switch (dest.id){
                            case "TH":
                                LineO.scale = .5;
                                break;
                            case "MM":
                                LineO.scale = 1.3;
                                break;
                        }
                    }
                    if (origin.id == "TH") {
                        switch (dest.id){
                            case "VN":
                                LineO.scale = .4;
                                break;
                            case "MM":
                                LineO.scale = 1.1;
                                break;
                            case "MY":
                                LineO.scale = .8;
                                break;
                            case "PH":
                                LineO.scale = 1.4;
                                break;
                            case "ID":
                                LineO.scale = 1.4;
                                break;
                        }
                    }
                    if (origin.id == "PH") {
                        LineO.scale = 1.6;
                        switch (dest.id) {
                            case "ID":
                                LineO.scale = 1.2;
                                break;
                            case "VN":
                                LineO.scale = 1.3;
                                break;
                            case "MM":
                                LineO.scale = 1.5;
                                break;

                        }
                    }
                    if (origin.id == "MY") {
                        LineO.scale = 1.3;
                        switch (dest.id) {
                            case "ID":
                                LineO.scale = 1;
                                break;
                            case "VN":
                                LineO.scale = 1;
                                break;
                            case "TH":
                                LineO.scale = .9;
                                break;
                            case "MM":
                                LineO.scale = 1.4;
                                break;
                            case "SG":
                                LineO.scale = .3;
                                break;
                        }
                    }
                    if (origin.id == "ID") {
                        LineO.scale = 1.5;
                        bulletLineO.height = 20;
                        switch (dest.id) {
                            case "PH":
                                LineO.scale = 1.4;
                                bulletLineO.height = 17;
                                break;
                            case "TH":
                                LineO.scale = 1.6;
                                break;
                            case "MM":
                                LineO.scale = 1.7;
                                break;
                            case "SG":
                                LineO.scale = 1.4;
                                break;
                        }
                    }
                    if (origin.id == "SG") {
                        switch (dest.id) {
                            case "PH":
                                LineO.scale = 1.4;
                                bulletLineO.height = 17;
                                break;
                            case "TH":
                                LineO.scale = 1.1;
                                break;
                            case "MM":
                                LineO.scale = 1.4;
                                break;
                            case "MY":
                                LineO.scale = .5;
                                break;
                            case "ID":
                                LineO.scale = .9;
                                break;
                        }
                    }
                }


                var bulletO = linesOrigin.lineObjects.create();
                var circleO = bulletO.createChild(am4core.Circle);
                circleO.fill = am4core.color("#FFF");
                circleO.strokeWidth = 1;
                circleO.zIndex = 8;
                circleO.stroke = am4core.color("#FFF");
                if (!detectmob()){
                    circleO.radius = 2;
                    bulletO.position = 1;
                }else{
                    circleO.radius = 1;
                    bulletO.position = 0.98;
                }
            }

            // DESTINATION MAP
            if(state == 'destination'){
                var lineSeriesTwo = chartTwo.series.push(new am4maps.MapLineSeries());
                lineSeriesTwo.interactionsEnabled = false;
                lineSeriesTwo.mapLines.template.shortestDistance = false; // keep the lines straight
                lineSeriesTwo.zIndex = 0;
                lineSeriesTwo.mapLines.template.visible = true;
                if (!detectmob()){
                    lineSeriesTwo.mapLines.template.line.strokeWidth = 1;
                } else{
                    lineSeriesTwo.mapLines.template.line.strokeWidth = 1;
                }

                lineSeriesTwo.mapLines.template.stroke = gradientDestination;

                var linesDest = lineSeriesTwo.mapLines.create();
                linesDest.data = [{
                    id: dest.id
                }];

                linesDest.multiGeoLine = [
                    coords
                ];


                // Add a map object to line
                var bulletLineD = linesDest.lineObjects.create();
                bulletLineD.nonScaling = true;
                bulletLineD.position = 1;
                bulletLineD.horizontalCenter = "middle";
                bulletLineD.verticalCenter = "middle";
                bulletLineD.fill = gradientDestination;
                bulletLineD.width = 70;

                var LineD = bulletLineD.createChild(am4core.Sprite);
                LineD.path = "M94 16 L99 12 L94 08 L4 12";
                LineD.opacity = 98;
                LineD.strokeOpacity = 0;

                if (!detectmob()){
                    bulletLineD.height = 72;
                    LineD.scale = 3;
                    LineD.dx = -268;
                    LineD.dy = 0;
                    if (origin.id == "MM"){
                        switch (dest.id){
                            case "VN":
                                LineD.scale = 2.5;
                                LineD.dx = -215;
                                bulletLineD.height = 65;
                                break;
                            case "TH":
                                LineD.scale = 2;
                                LineD.dx = -165;
                                bulletLineD.height = 48;
                                break;
                        }
                    }
                    if (origin.id == "TH"){
                        switch (dest.id){
                            case "MM":
                                LineD.scale = 1.8;
                                LineD.dx = -150;
                                bulletLineD.height = 45;
                                break;
                            case "VN":
                                LineD.scale = 1.55;
                                LineD.dx = -123;
                                bulletLineD.height = 40;
                                break;
                            case "MY":
                                LineD.scale = 2.5;
                                LineD.dx = -215;
                                bulletLineD.height = 60;
                                break;
                        }
                    }
                    if (origin.id == "VN"){
                        switch (dest.id){
                            case "MY":
                                LineD.scale = 2.3;
                                LineD.dx = -197;
                                bulletLineD.height = 54;
                                break;
                            case "SG":
                                LineD.scale = 2.3;
                                LineD.dx = -197;
                                bulletLineD.height = 54;
                                break;
                            case "TH":
                                LineD.scale = 1.55;
                                LineD.dx = -123;
                                bulletLineD.height = 38;
                                break;
                        }
                    }
                    if (origin.id == "MY"){
                        LineD.scale = 2.65;
                        LineD.dx = -231;
                        bulletLineD.height = 64;
                        switch (dest.id){
                            case "TH":
                                LineD.scale = 2.69;
                                LineD.dx = -235;
                                bulletLineD.height = 64;
                            case "ID":
                                LineD.scale = 1.55;
                                LineD.dx = -123;
                                bulletLineD.height = 40;
                                break;
                            case "SG":
                                LineD.scale = 1;
                                LineD.dx = -58;
                                bulletLineD.height = 23;
                                break;
                        }
                    }
                    if (origin.id == "SG"){
                        LineD.scale = 2.8;
                        LineD.dx = -245;
                        bulletLineD.height = 70;
                        switch (dest.id){
                            case "ID":
                                LineD.scale = 2.45;
                                LineD.dx = -210;
                                bulletLineD.height = 60;
                                break;
                            case "MY":
                                LineD.scale = 1;
                                LineD.dx = -58;
                                bulletLineD.height = 23;
                                break;
                            case "TH":
                                LineD.scale = 2.6;
                                LineD.dx = -228;
                                bulletLineD.height = 65;
                                break;
                        }
                    }
                }else{
                    bulletLineD.height = 40;
                    LineD.scale = 1.6;
                    LineD.dx = -130;
                    if (origin.id == "MM"){
                        switch (dest.id){
                            case "TH":
                                LineD.dx = -70;
                                LineD.scale = 1.2;
                                bulletLineD.height = 25;
                                break;
                            case "VN":
                                LineD.dx = -100;
                                LineD.scale = 1.3;
                                bulletLineD.height = 30;
                                break;
                        }
                    }
                    if (origin.id == "SG"){
                        switch (dest.id){
                            case "MY":
                                LineD.dx = -50;
                                LineD.scale = 0;
                                break;
                        }
                    }
                    if (origin.id == "MY"){
                        switch (dest.id){
                            case "SG":
                                LineD.dx = -50;
                                LineD.scale = 0;
                                break;
                        }
                    }
                    if (origin.id == "VN"){
                        switch (dest.id){
                            case "TH":
                                bulletLineD.height = 20;
                                LineD.dx = -80;
                                LineD.scale = 1;
                                break;
                            case "MY":
                                bulletLineD.height = 25;
                                LineD.dx = -90;
                                LineD.scale = 1.2;
                                break;
                        }
                    }
                    if (origin.id == "TH"){
                        switch (dest.id){
                            case "VN":
                                bulletLineD.height = 25;
                                LineD.dx = -65;
                                LineD.scale = 1;
                                break;
                        }
                    }
                }

                var bulletDest = linesDest.lineObjects.create();
                var circleD = bulletDest.createChild(am4core.Circle);
                circleD.fill = am4core.color("#7E7E7E");
                circleD.strokeWidth = 1;
                circleD.zIndex = 5;
                circleD.stroke = am4core.color("#7E7E7E");
                if (!detectmob()){
                    circleD.radius = 2;
                    bulletDest.position = 1;
                }else{
                    circleD.radius = 1;
                    bulletDest.position = 1;
                }

            }

        }
    })

}

function getOriginCountry(id){
    for(var i=0;i<originCountries.length;i++){
        if(originCountries[i].id === id){
            return originCountries[i]
        }
    }
}

function getDestinationCountry(id){
    for(var i=0;i<destinationCountries.length;i++){
        if(destinationCountries[i].id === id){
            return destinationCountries[i]
        }
    }
}

function getDestinationList(originId) {
    var origin = getOriginCountry(originId);
    var destList = [];
    if (origin) {
        origin.destinations.forEach(element => {
            for (var i = 0; i < destinationCountries.length; i++) {
                if (destinationCountries[i].id === element) {
                    destList.push(destinationCountries[i]);
                }
            }
        });
    }
    return destList;
}

function colorDeselectedCountry(countryCode){
    for(let i=0;i<polygonSeries2.children.values.length;i++){
        if(polygonSeries2.children.values[i].className === "MapPolygon"){
            if(polygonSeries2.children.values[i].dataItem.dataContext.id === countryCode){
                polygonSeries2.children.values[i].fill = am4core.color("#5A5F6A");
            }
        }
    }
    for(let i=0;i<polygonSeriesMini2.children.values.length;i++){
        if(polygonSeriesMini2.children.values[i].className === "MapPolygon"){
            if(polygonSeriesMini2.children.values[i].dataItem.dataContext.id === countryCode){
                polygonSeriesMini2.children.values[i].fill = am4core.color("#5A5F6A");
            }
        }
    }
    for(let i=0;i<polygonSeries2Two.children.values.length;i++){
        if(polygonSeries2Two.children.values[i].className === "MapPolygon"){
            if(polygonSeries2Two.children.values[i].dataItem.dataContext.id === countryCode){
                polygonSeries2Two.children.values[i].fill = am4core.color("#D8D8D8");
            }
        }
    }
    for(let i=0;i<polygonSeriesMini2Two.children.values.length;i++){
        if(polygonSeriesMini2Two.children.values[i].className === "MapPolygon"){
            if(polygonSeriesMini2Two.children.values[i].dataItem.dataContext.id === countryCode){
                polygonSeriesMini2Two.children.values[i].fill = am4core.color("#D8D8D8");
            }
        }
    }
}

function resetCountryColors(){
    if(!onCountry){
        for(let i=0;i<polygonSeries2.children.values.length;i++){
            if(polygonSeries2.children.values[i].className === "MapPolygon"){
                polygonSeries2.children.values[i].fill = am4core.color("#52C6D8");
            }
        }
        for(let i=0;i<polygonSeriesMini2.children.values.length;i++){
            if(polygonSeriesMini2.children.values[i].className === "MapPolygon"){
                polygonSeriesMini2.children.values[i].fill = am4core.color("#52C6D8");
            }
        }
        for(let i=0;i<polygonSeries2Two.children.values.length;i++){
            if(polygonSeries2Two.children.values[i].className === "MapPolygon"){
                polygonSeries2Two.children.values[i].fill = am4core.color("#4EC483");
            }
        }
        for(let i=0;i<polygonSeriesMini2Two.children.values.length;i++){
            if(polygonSeriesMini2Two.children.values[i].className === "MapPolygon"){
                polygonSeriesMini2Two.children.values[i].fill = am4core.color("#4EC483");
            }
        }
    }
}

function highlightSelectedCountry(countryCode){
    for(let i=1;i<polygonSeries2.children.values.length;i++){
        if(polygonSeries2.children.values[i].className === "MapPolygon"){
            if(polygonSeries2.children.values[i].dataItem.dataContext.id !== countryCode){
                polygonSeries2.children.values[i].fill = am4core.color("#5A5F6A");
            }else{
                polygonSeries2.children.values[i].fill = am4core.color("#52C6D8");
            }

        }
    }
    for(let i=0;i<polygonSeriesMini2.children.values.length;i++){
        if(polygonSeriesMini2.children.values[i].className === "MapPolygon"){
            if( onCountry && polygonSeriesMini2.children.values[i].dataItem.dataContext.id !== countryCode){
                polygonSeriesMini2.children.values[i].fill = am4core.color("#5A5F6A");
            }else{
                polygonSeriesMini2.children.values[i].fill = am4core.color("#52C6D8");
            }

        }
    }

    for(let i=0;i<polygonSeries2Two.children.values.length;i++){
        if(polygonSeries2Two.children.values[i].className === "MapPolygon"){
            if(polygonSeries2Two.children.values[i].dataItem.dataContext.id !== countryCode){
                polygonSeries2Two.children.values[i].fill = am4core.color("#D8D8D8");
            }else{
                polygonSeries2Two.children.values[i].fill = am4core.color("#4EC483");
            }

        }
    }
    for(let i=0;i<polygonSeriesMini2Two.children.values.length;i++){
        if(polygonSeriesMini2Two.children.values[i].className === "MapPolygon"){
            if(polygonSeriesMini2Two.children.values[i].dataItem.dataContext.id !== countryCode){
                polygonSeriesMini2Two.children.values[i].fill = am4core.color("#D8D8D8");
            }else{
                polygonSeriesMini2Two.children.values[i].fill = am4core.color("#4EC483");
            }

        }
    }
}

function ShowInformation_country(isoCode){
    // Top Pickups information
    for (var data2 in json) {
        if (country === json[data2].iso && state == 'origin') {
            for (var i = 0; i < json[data2].origin.countries.length; i++) {
                if (json[data2].origin.countries[i].isoB === isoCode){
                    // Top Pickups

                    $("#pickups-1").html("<div class=\"headerPickUp\"><b>Top pick-up points in " +
                        (( isoCode === "PH") ? 'the ' : '') +
                        json[data2].origin.countries[i].country +
                        "</b></br>Grab users from " +
                        ((json[data2].country === "Philippines") ? 'the ': '') +
                        json[data2].country +
                        "</div>");
                    $("#pickups-1").append('<div class=\"itemPickup\"> <img src="' + json[data2].origin.countries[i].info_below_menu[0].src + '" alt=\"\" /> <span>' + json[data2].origin.countries[i].info_below_menu[0].text + '</span></div>');
                    $("#pickups-1").append('<div class=\"itemPickup\"> <img src="' + json[data2].origin.countries[i].info_below_menu[1].src + '" alt=\"\" /> <span>' + json[data2].origin.countries[i].info_below_menu[1].text + '</span></div>');
                    $("#pickups-1").append('<div class=\"itemPickup\"> <img src="' + json[data2].origin.countries[i].info_below_menu[2].src + '" alt=\"\" /> <span>' + json[data2].origin.countries[i].info_below_menu[2].text + '</span></div>');
                    return;
                }
            }
        } else {
            if (country == json[data2].iso && state == 'destination') {
                for (var i = 0; i < json[data2].destinations.countries.length; i++) {
                    if (json[data2].destinations.countries[i].isoB === isoCode){
                        // Top Pickups
                        if( isoCode === "PH"){
                            $("#pickups").html("<div class=\"headerPickUp\"><b>Top pick-up points in " + json[data2].country  + "</b></br>Grab users from the " + json[data2].destinations.countries[i].country + "</div>");
                        }else if( json[data2].country === "Philippines"){
                            $("#pickups").html("<div class=\"headerPickUp\"><b>Top pick-up points in the " + json[data2].country  + "</b></br>Grab users from " + json[data2].destinations.countries[i].country + "</div>");
                        }else{
                            $("#pickups").html("<div class=\"headerPickUp\"><b>Top pick-up points in " + json[data2].country  + "</b></br>Grab users from " + json[data2].destinations.countries[i].country + "</div>");
                        }
                        $("#pickups").html("<div class=\"headerPickUp\"><b>Top pick-up points in " +
                            ((json[data2].country === "Philippines") ? 'the ': '') +
                            json[data2].country  +
                            "</b></br>Grab users from " +
                            (( isoCode === "PH") ? 'the ' : '') +
                            json[data2].destinations.countries[i].country +
                            "</div>");
                        $("#pickups").append("<div class=\"itemPickup\"> <img src=\" " + json[data2].destinations.countries[i].info_below_menu[0].src + "\" alt=\"\" /> <span>" + json[data2].destinations.countries[i].info_below_menu[0].text + "</span></div>");
                        $("#pickups").append("<div class=\"itemPickup\"> <img src=\" " + json[data2].destinations.countries[i].info_below_menu[1].src + "\" alt=\"\" /> <span>" + json[data2].destinations.countries[i].info_below_menu[1].text + "</span></div>");
                        $("#pickups").append("<div class=\"itemPickup\"> <img src=\" " + json[data2].destinations.countries[i].info_below_menu[2].src + "\" alt=\"\" /> <span>" + json[data2].destinations.countries[i].info_below_menu[2].text + "</span></div>");
                        return;
                    }
                }
            }
        }
    }
};

function ShowInformation_pie(isoCode){
    // Chart Percentage information
    for (var data2 in json) {
        if (country === json[data2].iso && state == 'origin') {
            for (var i = 0; i < json[data2].origin.countries.length; i++) {
                if (json[data2].origin.countries[i].isoB === isoCode){
                    // Percentage Information
                    if (detectmob()) {
                        $("#info-chart-1").html("<div class=\"percentage\"><div class=\"number\">"+ json[data2].origin.countries[i].percentage +"</div> <div class=\"signal\">%</div><div class=\"divider\"> |</div></div><div class=\"text\">" +
                            json[data2].country + " Grab users booked  " + json[data2].origin.countries[i].percentage + "% </br> of their overseas rides in " +
                            ((json[data2].origin.countries[i].country === "Philippines") ? 'the ': '') +
                            json[data2].origin.countries[i].country +
                            "</div>");
                    }else{
                        $("#info-chart-1").html("<div class=\"percentage\"><div class=\"number\">"+ json[data2].origin.countries[i].percentage +"</div> <div class=\"signal\">%</div><div class=\"divider\"> |</div></div><div class=\"text\">" +
                            json[data2].country + " Grab users booked  " + json[data2].origin.countries[i].percentage + "% </br> of their overseas rides in " +
                            ((json[data2].origin.countries[i].country === "Philippines") ? 'the ': '') +
                            json[data2].origin.countries[i].country +
                            "</div>");
                    }

                    return;
                }
            }
        } else {
            if (country == json[data2].iso && state == 'destination') {
                for (var i = 0; i < json[data2].destinations.countries.length; i++) {
                    if (json[data2].destinations.countries[i].isoB === isoCode){
                        // Percentage Information
                        if (detectmob()){
                            $("#info-chart").html("<div class=\"percentage\"><div class=\"number\">"+ json[data2].destinations.countries[i].percentage +"</div> <div class=\"signal\">%</div><div class=\"divider\"> |</div></div><div class=\"text\">" +
                                ((json[data2].destinations.countries[i].country === "Philippines") ? 'the ': '') +
                                json[data2].destinations.countries[i].country + " Grab users made up " + json[data2].destinations.countries[i].percentage + "% </br> of all overseas rides in " +
                                ((json[data2].country === "Philippines") ? 'the ': '') +
                                json[data2].country +
                                "</div>");
                        }else{
                            $("#info-chart").html("<div class=\"percentage\"><div class=\"number\">"+ json[data2].destinations.countries[i].percentage +"</div> <div class=\"signal\">%</div><div class=\"divider\"> |</div></div><div class=\"text\">" +
                                ((json[data2].destinations.countries[i].country === "Philippines") ? 'the ': '') +
                                json[data2].destinations.countries[i].country + " Grab users made up " + json[data2].destinations.countries[i].percentage + "% </br> of all overseas rides in " +
                                ((json[data2].country === "Philippines") ? 'the ': '') +
                                json[data2].country +
                                "</div>");
                        }
                        return;
                    }
                }
            }
        }
    }
};

function colorSelectedCountry(countryCode){
    // Solo se usa para el hover del slice en el pie chart.
    for(let i=0;i<polygonSeries2.children.values.length;i++){
        if(polygonSeries2.children.values[i].className === "MapPolygon"){
            if(polygonSeries2.children.values[i].dataItem.dataContext.id === countryCode) {
                polygonSeries2.children.values[i].fill = am4core.color("#4EC483");
            }
        }
    }
    for(let i=0;i<polygonSeriesMini2.children.values.length;i++){
        if(polygonSeriesMini2.children.values[i].className === "MapPolygon"){
            if(polygonSeriesMini2.children.values[i].dataItem.dataContext.id === countryCode){
                polygonSeriesMini2.children.values[i].fill = am4core.color("#4EC483");
            }
        }
    }
    for(let i=0;i<polygonSeries2Two.children.values.length;i++){
        if(polygonSeries2Two.children.values[i].className === "MapPolygon"){
            if(polygonSeries2Two.children.values[i].dataItem.dataContext.id === countryCode){
                polygonSeries2Two.children.values[i].fill = am4core.color("#52C6D8");
                polygonSeries2Two.children.values[i].opacity = 0.9;
            }
        }
    }
    for(let i=0;i<polygonSeriesMini2Two.children.values.length;i++){
        if(polygonSeriesMini2Two.children.values[i].className === "MapPolygon"){
            if(polygonSeriesMini2Two.children.values[i].dataItem.dataContext.id === countryCode){
                polygonSeriesMini2Two.children.values[i].fill = am4core.color("#52C6D8");
            }
        }
    }
}

function load(countryCode){
    //zoomToHome();
    zoomToPlace();
    highlightSelectedCountry(countryCode);
    drawDestinationPoints(countryCode);
    drawLines(countryCode, false);
}

function onCountryClick(ev){
    onCountry = true;
    if (detectmob()){
        $.appearNavBar();
    }

    var countryCode = ev.target.dataItem.dataContext.id;
    country = countryCode;
    selectedCountry = countryCode;
    $.changeCountry(state, countryCode, "");
    setTimeout(function(){
        load(countryCode);
    }, 100);
    changeGraphData(countryCode);
    showGraph(countryCode);
    for (var data2 in json) {
        if(countryCode === json[data2].iso){
            countryName = json[data2].country.toUpperCase();
        }
    }

    if(state == 'origin'){
        activeState.properties.fill = am4core.color("#0f8ea2");
        hoverState.properties.fill = am4core.color("#0f8ea2");
        label2.text = countryName;
        $("#info-chart-1").hide();
        $("#pickups-1").hide();
    }

    if (state == 'destination') {
        activeState2.properties.fill = am4core.color("#00AD5E");
        hoverState2.properties.fill = am4core.color("#00AD5E");
        label2Two.text = countryName;
        $("#info-chart").hide();
        $("#pickups").hide();
    }
}

function UnselectCountry(){
    onCountry = false;

    // Destination country
    for(let poly of polygonSeries2Two.children.values){
        poly.isActive = false;
    }

    for(let poly2 of polygonSeriesMini2Two.children.values){
        poly2.isActive = false;
    }

    dimageSeriesTwo.data = [];
    for(var i=0; i<chartTwo.series.values.length;i++){
        if(chartTwo.series.values[i].className === "MapLineSeries"){
            chartTwo.series.removeIndex(i).dispose();
            i=0;
        }
    }

    $("#piechart-2").css({ visibility: "hidden"});


    // Origin country
    for(let poly of polygonSeries2.children.values){
        poly.isActive = false;
    }

    for(let poly2 of polygonSeriesMini2.children.values){
        poly2.isActive = false;
    }

    dimageSeries.data = [];
    for(var i=0; i<chart.series.values.length;i++){
        if(chart.series.values[i].className === "MapLineSeries"){
            chart.series.removeIndex(i).dispose();
            i=0;
        }
    }

    $("#piechart-1").css({ visibility: "hidden"});

    $("#info-chart").hide();
    $("#pickups").hide();
    $("#info-chart-1").hide();
    $("#pickups-1").hide();
    setTimeout(function(){
        lineSingaporeFunct();
        if (state == 'destination'){
            chartTwo.goHome();
        }else{
            chart.goHome();
        }
    }, 400);
    $.changeCountry(state,"","");
    resetCountryColors();
    zoomToHome();
}

function zoomToPlace(){
    if (state === 'origin'){
        chart.goHome();
    }else{
        chartTwo.goHome();
    }

}

function zoomToHome(){
    runUpdate = false;
    savedCode = '';
    if (state == 'origin'){
        chart.goHome();
    }else{
        chartTwo.goHome();
    }

    runUpdate = true;
}

function lineSingaporeFunct(){
    if (state == 'destination') {
        lineSingaporeDestination = chartTwo.series.push(new am4maps.MapLineSeries());
        lineSingaporeDestination.interactionsEnabled = false;
        lineSingaporeDestination.mapLines.template.shortestDistance = false; // keep the lines straight
        lineSingaporeDestination.mapLines.template.visible = true;
        lineSingaporeDestination.mapLines.template.stroke = am4core.color("#000");
        lineSingaporeDestination.mapLines.template.line.strokeWidth = 0.3;
        lineSingaporeDestination.zIndex = 1;

        lineSingaporeDestination.data = [{
            "multiGeoLine": [
                coords_sing
            ]
        }];
    }else{
        lineSingaporeOrigin = chart.series.push(new am4maps.MapLineSeries());
        lineSingaporeOrigin.interactionsEnabled = false;
        lineSingaporeOrigin.mapLines.template.shortestDistance = false; // keep the lines straight
        lineSingaporeOrigin.mapLines.template.visible = true;
        lineSingaporeOrigin.mapLines.template.stroke = am4core.color("#fff");
        if (detectmob()){
            lineSingaporeOrigin.mapLines.template.line.strokeWidth = 0.3;
        }else{
            lineSingaporeOrigin.mapLines.template.line.strokeWidth = 1;
        }

        lineSingaporeOrigin.zIndex = 1;
        lineSingaporeOrigin.data = [{
            "multiGeoLine": [
                coords_sing
            ]
        }];
    }
}

function showGraph(countryCode){
    deselectAllPieOrigin();
    deselectAllPieDestination();
    savedCode = countryCode;
    var country = getOriginCountry(countryCode);
    var xy;
    (state == 'origin') ?  xy = chart.geoPointToSVG({longitude: country.longitude, latitude: country.latitude}) : xy = chartTwo.geoPointToSVG({longitude: country.longitude, latitude: country.latitude});
    $("#piechart-1").css( {position:"absolute", left:xy.x - 100,top:xy.y-100, visibility: "visible"});
    $("#piechart-2").css( {position:"absolute", left:xy.x - 100,top:xy.y-100, visibility: "visible"});
}

function updateGraph(){
    if(runUpdate) {
        var country = getOriginCountry(savedCode);
        var xy;
        if (country) {
            (state == 'origin') ?  xy = chart.geoPointToSVG({longitude: country.longitude, latitude: country.latitude}) : xy = chartTwo.geoPointToSVG({longitude: country.longitude, latitude: country.latitude});
            $("#piechart-1").css({position: "absolute", left: xy.x - 100, top: xy.y - 100, visibility: "visible"});
            $("#piechart-2").css({position: "absolute", left: xy.x - 100, top: xy.y - 100, visibility: "visible"});
        }
        if (detectmob() && screen.width < 400 && screen.height < 670 && selectedCountry == 'MM'){
            $("#piechart-1").css('left', '+=20');
            $("#piechart-1").css('top', '+=20');
            $("#piechart-2").css('top', '+=20');
            $("#piechart-2").css('left', '+=20');
        }
    }
}
function changeGraphData(countryCode){
    for(var data in json){
        if(countryCode === json[data].iso){
            if(state === 'origin'){
                chart2.data = json[data].origin.countries;
            }
            else
            {
                chart2Two.data = json[data].destinations.countries;
            }
        }
    };
}
chart.events.on( "mappositionchanged", updateGraph );
chartTwo.events.on( "mappositionchanged", updateGraph );

function detectmob() {
    if( navigator.userAgent.match(/Android/i)
        || navigator.userAgent.match(/webOS/i)
        || navigator.userAgent.match(/iPhone/i)
        || navigator.userAgent.match(/iPad/i)
        || navigator.userAgent.match(/iPod/i)
        || navigator.userAgent.match(/BlackBerry/i)
        || navigator.userAgent.match(/Windows Phone/i)
    ){
        return true;
    }
    else {
        return false;
    }
}